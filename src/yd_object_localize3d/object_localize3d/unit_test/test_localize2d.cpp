#include <algorithm>
#include <fstream>
#include <iostream>

#include <opencv2/opencv.hpp>
#include<pcl/io/pcd_io.h>

#include "ros/ros.h"
#include "ros/time.h"
#include "rosbag/bag.h"
#include "rosbag/view.h"

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "person_pose2d_msgs/PersonPose2d.h"

#include "person_localize2d/libperson_localize2d.h"
#include "person_localize2d/person_localize2d_interface.h"

#include "person_angle/person_angle.h"
#include "common/person_joints.h"


DEFINE_string(bag_filename, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(calibration_filepath, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(image_directory, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");


using namespace std;
using namespace cv;
using namespace person_localize2d;


namespace object_location {
using namespace common;

namespace  object_localize3d{
namespace {
constexpr int kExtendPixels = 20;

enum ObjectType{
  PERSON = 0,
  VEHICLE = 1,

};


vector<Point2i> ConvertToPersonJoint(const geometry_msgs::Polygon::_points_type& raw_joints) {
  vector<Point2i> person_joints((int)PersonJointOrder::TOTAL_JOINTS);

  auto raw_joint = raw_joints[(int)RawJointOrder::HEAD];
  person_joints[(int)PersonJointOrder::HEAD] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::NECK];
  person_joints[(int)PersonJointOrder::NECK] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::LEFT_SHOULDER];
  person_joints[(int)PersonJointOrder::LEFT_SHOULDER] = Point2f((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::RIGHT_SHOULDER];
  person_joints[(int)PersonJointOrder::RIGHT_SHOULDER] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::LEFT_WAIST];
  person_joints[(int)PersonJointOrder::LEFT_WAIST] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::RIGHT_WAIST];
  person_joints[(int)PersonJointOrder::RIGHT_WAIST] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::LEFT_KNEE];
  person_joints[(int)PersonJointOrder::LEFT_KNEE] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::RIGHT_KNEE];
  person_joints[(int)PersonJointOrder::RIGHT_KNEE] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::LEFT_ANKLE];
  person_joints[(int)PersonJointOrder::LEFT_ANKLE] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::RIGHT_ANKLE];
  person_joints[(int)PersonJointOrder::RIGHT_ANKLE] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::LEFT_HEEL];
  person_joints[(int)PersonJointOrder::LEFT_HEEL] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::RIGHT_HEEL];
  person_joints[(int)PersonJointOrder::RIGHT_HEEL] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::LEFT_TOE];
  person_joints[(int)PersonJointOrder::LEFT_TOE] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::RIGHT_TOE];
  person_joints[(int)PersonJointOrder::RIGHT_TOE] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::LEFT_SOLES];
  person_joints[(int)PersonJointOrder::LEFT_SOLES] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::RIGHT_SOLES];
  person_joints[(int)PersonJointOrder::RIGHT_SOLES] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  return person_joints;
}

vector<int16_t> ConvertToPersonJointOfPersonAngle(const geometry_msgs::Polygon::_points_type& raw_joints) {
  vector<int16_t> person_joints;
  for (size_t i  = 0; i <  raw_joints.size(); i++) {
    person_joints.push_back((int16_t)round(raw_joints[i].x));
    person_joints.push_back((int16_t)round(raw_joints[i].y));
  }
  return person_joints;
}
vector<Point2i> ConvertToPersonJointOfPersonAngle1(const geometry_msgs::Polygon::_points_type& raw_joints) {
  vector<Point2i> person_joints;
  for (size_t i  = 0; i <  raw_joints.size(); i++) {
    person_joints.push_back(Point2i(round(raw_joints[i].x),round(raw_joints[i].y) ));
  }
  return person_joints;
}

// Point2f GetDirectionPoint(const Point2f& origin, float angle) {
//   return origin + Point2f(kExtendPixels * cos(angle), kExtendPixels * sin(angle));
// }
// int LoadIntrinsic(const string &calibration_filepath_, Mat& intrinsic_mat, Mat& distortion_coeffs, Mat& extrinsic_mat)
//   {
//   cv::FileStorage fs;

//   fs.open(calibration_filepath_, cv::FileStorage::READ);
//     if (!fs.isOpened())
//   {
//     LOG(FATAL) << "can't open calibration file.";
//     return -1;
//   }

//     fs["CameraMat"] >> intrinsic_mat;
//     fs["DistCoeff"] >> distortion_coeffs;
//     fs["CameraExtrinsicMat"] >> extrinsic_mat;

//     return 0;
// }

void Run( ) {
  auto person2d_localizer_ = CreatePerson2dLocalizer( );
  auto person_angle_ = make_shared<PersonAngleLib::PersonAngle>();


  int result = person_angle_->LoadCalibrationFile(FLAGS_calibration_filepath);
  
  
  rosbag::Bag bag;
  bag.open(FLAGS_bag_filename, rosbag::bagmode::Read);
  rosbag::View view(bag);
  const ::ros::Time begin_time = view.getBeginTime();
  const double duration_in_seconds =
        (view.getEndTime() - begin_time).toSec();
  for (const rosbag::MessageInstance& message : view) {
    if(message.isType<person_pose2d_msgs::PersonPose2d>()) {
      auto msg = message.instantiate<person_pose2d_msgs::PersonPose2d>();

      // if (msg->header.stamp.sec == 1653446733) {
      //   if ( msg->header.stamp.nsec == 207285905)
      //     LOG(INFO) << "time stamp :" << msg->header.stamp.sec << ": " << msg->header.stamp.nsec;
      // }
      if (msg->object_types.empty())
        return;
        
      string image_filepath = FLAGS_image_directory + "/" + to_string(msg->header.stamp.sec)+ "_" + to_string(msg->header.stamp.nsec) + ".jpg";
      LOG(INFO) << "image file path:" << image_filepath;
          
      cv::Mat image = cv::imread(image_filepath);
      if (image.empty()){
        LOG(ERROR) << "Failed to open " << image_filepath;
        continue;
      }

        for (size_t i = 0; i < msg->object_types.size(); i++) {
        //person
        if (msg->object_types[i] == 1) {
          
          const auto& person_poses = ConvertToPersonJoint(msg->person_poses[i].points);
          cv::Rect2i person_box(0, 0, 0, 0);
          if (msg->person_poses[i].points.size() == (int)RawJointOrder::TOTAL_JOINTS + 2){
            static double dMExtendScale = 0.2;
            person_box.x = (int)round(msg->person_poses[i].points[ (int)RawJointOrder::TOTAL_JOINTS].x);
            person_box.y = (int)round(msg->person_poses[i].points[ (int)RawJointOrder::TOTAL_JOINTS].y);
            person_box.width = (int)round(msg->person_poses[i].points[ (int)RawJointOrder::TOTAL_JOINTS + 1].x -  person_box.x);
            person_box.height = (int)round(msg->person_poses[i].points[ (int)RawJointOrder::TOTAL_JOINTS + 1].y -  person_box.y);
            person_box.width = person_box.width  /  (1 + 2*dMExtendScale);
            person_box.height = person_box.height  /  (1 + 2*dMExtendScale);
            person_box.x = person_box.x +  person_box.width * dMExtendScale;
            person_box.y = person_box.y +  person_box.height * dMExtendScale;
          }  

          for (int j = 0;  j < person_poses.size(); j++) {
            Point2f joint(person_poses[j].x, person_poses[j].y);
            circle(image, joint, 5, Scalar(0u, 255u, 0u));
          }
          // if (person_box.width > 0) {
          //       rectangle(image, person_box, Scalar(255u, 0u, 0u), 5);
          // }
          
          Point2i person_projection_on_floor;

          int result =  person2d_localizer_->Localize(person_poses, person_box, person_projection_on_floor);
          if (0 != result) {
            return;
          }
          circle(image, person_projection_on_floor, 5, Scalar(0u,  0u,255u));

          // auto person_poses_angle = ConvertToPersonJointOfPersonAngle(msg->person_poses[i].points);
          // float person_angle = person_angle_->ComputeVAngle(person_poses_angle);
          auto person_joints = ConvertToPersonJointOfPersonAngle1(msg->person_poses[i].points); 

         Vec3f person_orientation;
          person_angle_->ComputeOrientation(person_joints, person_orientation);
           Vec2f person_orientation2d_in_cam;
           if (person_angle_->Compute2DOrientation(person_joints, person_orientation2d_in_cam)) {
              Point2i person_forward_point2d(person_projection_on_floor + Point2i(person_orientation2d_in_cam[0], person_orientation2d_in_cam[1]));
              arrowedLine(image, person_projection_on_floor, person_forward_point2d, Scalar(0u,  0u,255u, 5));
           }
         //Vec2f person_orientation_2d(person_orientation[0], person_orientation[1]);
         //person_orientation_2d = normalize(person_orientation_2d);


        }
      }
      imwrite( image_filepath + ".png", image);

    }
  }

}
void Test(int argc, char** argv) {

    Run();

}

}
}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  CHECK(!FLAGS_bag_filename.empty())
      << "-configuration_directory is missing.";

  CHECK(!FLAGS_image_directory.empty())
      << "-map_configuration_basename is missing.";


  object_location::object_localize3d::Test(argc, argv);

}
