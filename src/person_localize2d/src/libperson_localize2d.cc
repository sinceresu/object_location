#include "person_localize2d/libperson_localize2d.h"
#include "person2d_localizer.h"

namespace person_localize2d{

std::shared_ptr<PersonLocalize2dInterface> CreatePerson2dLocalizer()
{
    return std::shared_ptr<PersonLocalize2dInterface>(new Person2dLocalizer());
}

} // namespace person_localize2d
