#pragma once

#include "object_localizer.h"

namespace object_location{

namespace object_localize3d{
  
class PersonLocalizer  : public ObjectLocalizer
{
public:
  explicit PersonLocalizer() ;

  virtual ~PersonLocalizer();

  PersonLocalizer(const PersonLocalizer&) = delete;
  PersonLocalizer& operator=(const PersonLocalizer&) = delete;

private:
  virtual void GetObjectPosition(const cv::Rect2f&  object_region, cv::Point2f&  object_position2d, double & object_height);

  bool IsWholeBody(const cv::Rect2f& object_region);
  bool IsAtBottom(const cv::Rect2f& object_region);
  

};
} // namespaceobject_location
}// namespaceobject_location
