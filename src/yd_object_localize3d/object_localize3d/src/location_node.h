#pragma once

#include <map>
#include <memory>
#include <vector>

#include <opencv2/opencv.hpp>

#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "person_pose2d_msgs/PersonPose2d.h"
#include "object_localize3d_msgs/ObjectPose.h"

namespace person_localize2d{
class PersonLocalize2dInterface;

}
namespace PersonAngleLib {
class PersonAngle;

}


namespace object_location {
namespace  object_localize3d{

class CamParamReader;
class RotateCamParamReader;

class ObjectLocalizer;
class FloorExtractor;

class LocationNode {
 public:
   struct LocationNodeOptions {
    bool ptz_rotate;
    std::string calibration_filepath;
    std::string cloud_map_filepath;
    std::string ptz_pose_topic;
    std::string detect_topic;
    std::string poseKeypoints_topic;
    std::string location_topic;
    std::string frame_id;
    float horizontal_fov;
    float vertical_fov;
    float near_plane_distance;
    float far_plane_distance;
    std::vector<int> object_types;
    std::vector<std::string> object_labels;
    std::vector<float> object_heights;

    bool extract_floor;
    bool extract_only_floor;
    bool use_normal_filtering;
    bool use_upsample_filtering;
    float sensor_height;
    float height_clip_range;
  };

  LocationNode(const LocationNodeOptions& options);
  ~LocationNode();

  LocationNode(const LocationNode&) = delete;
  LocationNode& operator=(const LocationNode&) = delete;

  private:
    void LaunchPublishs();
    void LaunchSubscribers();
    void PtzPoseCallback(const std::string &sensor_id,
                         const nav_msgs::Odometry::ConstPtr &msg);
    // void HandleDetectionMessage(const std::string& sensor_id,
    //                           const object_localize3d_msgs::ObjectDetect::ConstPtr& msg);
    void HandlePoseKeypointsMessage(const std::string& sensor_id,
                              const person_pose2d_msgs::PersonPose2d::ConstPtr& msg);
    bool GetPerson3dPose(const ::geometry_msgs::Polygon & key_points, ::geometry_msgs::Pose & pose);

    cv::Vec3f GetPerson3dOrientation(const std::vector<cv::Point2i> & person_joints, const cv::Point2f& person_position2d, const cv::Point3f& person_position3d);

    void ptz_pose_update(const nav_msgs::Odometry::ConstPtr &msg);

    LocationNodeOptions node_options_;
    ::ros::NodeHandle node_handle_;
    ::ros::Publisher pose_position_publisher_;
    // rviz
    ::ros::Publisher global_map_publisher_;
    ::ros::Publisher camera_map_publisher_;
    ::ros::Publisher obj_pose_publisher_;

    ::ros::Subscriber ptz_pose_subscriber_;
    ::ros::Subscriber detect_subscriber_;
    ::ros::Subscriber poseKeypoints_subscriber_;
    std::map<int, std::shared_ptr<ObjectLocalizer>> pose_localizers_;
    std::map<int, std::string> type_to_label_;
    int person_object_type_;

    std::shared_ptr<person_localize2d::PersonLocalize2dInterface> person2d_localizer_;;
    std::shared_ptr<PersonAngleLib::PersonAngle> person_angle_;;
    std::shared_ptr<FloorExtractor> floor_extractor_;

    std::shared_ptr<CamParamReader> cam_param_reader_;

  };
  
}
}