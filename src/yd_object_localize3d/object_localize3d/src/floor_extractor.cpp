#include "floor_extractor.h"

#include <math.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/search/impl/search.hpp>
#include <pcl/filters/impl/plane_clipper3D.hpp>
#include <pcl/filters/extract_indices.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/filters/passthrough.h>
#include <pcl/surface/mls.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include "glog/logging.h"

using namespace std;

namespace object_location {

namespace object_localize3d {
namespace {

/**
 * @brief plane_clip
 * @param src_cloud
 * @param plane
 * @param negative
 * @return
 */
PointCloud::Ptr plane_clip(const PointCloud::ConstPtr src_cloud,  float min_z, float max_z)
{

  PointCloud::Ptr dst_cloud(new PointCloud);

  pcl::PassThrough<PointType> pass;
  pass.setInputCloud (src_cloud);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (min_z, max_z);
  //pass.setFilterLimitsNegative (true);
  pass.filter (*dst_cloud);

  return dst_cloud;
}
void FilterPCLWithSor(PointCloud::ConstPtr input_pcl, PointCloud::Ptr output_pcl, int mean_k, double std_mul) {
  pcl::StatisticalOutlierRemoval <PointType> sor_filter;
  sor_filter.setInputCloud (input_pcl);
  sor_filter.setMeanK (mean_k);
  sor_filter.setStddevMulThresh (std_mul);
  sor_filter.filter (*output_pcl);
}
PointCloud::Ptr UpsamplingPointCloud(PointCloud::ConstPtr point_cloud) {
  PointCloud::Ptr upsampled(new PointCloud);

  pcl::MovingLeastSquares<PointType, PointType> filter;
  filter.setInputCloud(point_cloud);
  //建立搜索对象
  pcl::search::KdTree<PointType>::Ptr kdtree;
  filter.setSearchMethod(kdtree);
  //设置搜索邻域的半径为30cm
  filter.setSearchRadius(0.3);
  // Upsampling 采样的方法有 DISTINCT_CLOUD, RANDOM_UNIFORM_DENSITY
  filter.setUpsamplingMethod(pcl::MovingLeastSquares<PointType, PointType>::VOXEL_GRID_DILATION);
  // 采样的半径是0.2
  // filter.setUpsamplingRadius(0.2);
  // 采样步数的大小 
  // filter.setUpsamplingStepSize(0.05);
// filter.setPointDensity(1000);

  filter.setDilationVoxelSize(0.01);
  filter.process(*upsampled);

  return upsampled;
}


}

FloorExtractor::FloorExtractor()
{

}

int FloorExtractor::Extract(PointCloud::Ptr input_pcl, PointCloud::Ptr output_pcl) {
  pcl::VoxelGrid<PointType> vg;
  PointCloud::Ptr cloud_filtered (new  PointCloud);
  vg.setInputCloud (input_pcl);
  vg.setLeafSize (0.02f, 0.02f, 0.02f);
  vg.filter (*cloud_filtered);

  // compensate the tilt rotation
  Eigen::Matrix4f tilt_matrix = Eigen::Matrix4f::Identity();
  tilt_matrix.topLeftCorner(3, 3) = Eigen::AngleAxisf(param_.tilt_deg * M_PI / 180.0f, Eigen::Vector3f::UnitY()).toRotationMatrix();

  // filtering before RANSAC (height and normal filtering)
  PointCloud::Ptr filtered(new PointCloud);
  pcl::transformPointCloud(*cloud_filtered, *filtered, tilt_matrix);
  // filtering before RANSAC (height and normal filtering)
  filtered = plane_clip(filtered,  -param_.sensor_height - param_.height_clip_range,  -param_.sensor_height + param_.height_clip_range);
  // filtered = plane_clip(filtered, Eigen::Vector4f(0.0f, 0.0f, 1.0f, param_.sensor_height + param_.height_clip_range), false);
  // filtered = plane_clip(filtered, Eigen::Vector4f(0.0f, 0.0f, 1.0f, param_.sensor_height - param_.height_clip_range), true);

  if (param_.use_normal_filtering) {
    filtered = normal_filtering(filtered);
  }
  
  pcl::transformPointCloud(*filtered, *filtered,
                           static_cast<Eigen::Matrix4f>(tilt_matrix.inverse()));
  // too few points for RANSAC
  if (filtered->size() < 50) {
    return -1;
  }

//  auto segmentations =  SegPointcloud(filtered); 

//   PointCloud::Ptr segment_pcl (new PointCloud());

//  for (size_t i = 0; i < segmentations.size(); i++) {
//   *segment_pcl += *segmentations[i];
//  }
//   pcl::io::savePCDFileBinary("segment_pcl.pcd", *segment_pcl);
  pcl::io::savePCDFileBinary("filtered.pcd", *filtered);

  PointCloud::Ptr all_floor_pcl (new PointCloud());
  int i=0, nr_points = (int) filtered->size ();
  while (filtered->size() > 0.1 * nr_points){
  // size_t iters = param_.extract_only_floor ? 1 : 20;
  // for (size_t i = 0; i < iters; i++) {
    PointCloud::Ptr non_floor_pcl (new PointCloud());
     PointCloud::Ptr floor_pcl  = FilterFloor(filtered, non_floor_pcl);
    if (floor_pcl->size() > 20) {
          // pcl::io::savePCDFileBinary("floor_" + to_string(i) + ".pcd", *floor_pcl);
        *all_floor_pcl +=  *floor_pcl;
    }

    *filtered =  *non_floor_pcl;

    if (param_.extract_only_floor) break;
  }


  // pcl::io::savePCDFileBinary("floor_pcl.pcd", *all_floor_pcl);
//  *output_pcl = *all_floor_pcl;
  //  pcl::io::savePCDFileBinary("output.pcd", *output_pcl);

  // pcl::io::savePCDFileBinary("non_floor_pcl.pcd", *filtered);

// //  * output_pcl = *filtered;
// //  return 0;
//   PointCloud::Ptr all_cloud(new PointCloud);

//  auto segmentations =  SegPointcloud(filtered); 
//  for (size_t i = 0; i < segmentations.size(); i++) {
//   pcl::io::savePCDFileBinary("regions_seg_" + to_string(i) + ".pcd", *segmentations[i]);

//   pcl::SampleConsensusModelPlane<PointType>::Ptr model_p(
//       new pcl::SampleConsensusModelPlane<PointType>(segmentations[i]));
//   pcl::RandomSampleConsensus<PointType> ransac(model_p);
//   ransac.setDistanceThreshold(0.1);
//   ransac.computeModel();

//   pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
//   ransac.getInliers(inliers->indices);

//   // too few inliers
//   if (inliers->indices.size() < 30) {
//     continue;
//   }

//   // verticality check of the detected floor's normal
//   Eigen::Vector4f reference = tilt_matrix.inverse() * Eigen::Vector4f::UnitZ();

//   Eigen::VectorXf coeffs;
//   ransac.getModelCoefficients(coeffs);

//   double dot = coeffs.head<3>().dot(reference.head<3>());
//   if (std::abs(dot) < std::cos(param_.floor_normal_thresh * M_PI / 180.0)) {
//    continue;
//   }

//   PointCloud::Ptr cloud_projected(new PointCloud);
//   PointCloud::Ptr inlier_cloud(new PointCloud);

//   pcl::ExtractIndices<PointType> extract;
//   extract.setInputCloud(segmentations[i]);
//   extract.setIndices(inliers);
//   extract.filter(*inlier_cloud);

//   pcl::ProjectInliers<PointType> proj;
//   proj.setModelType (pcl::SACMODEL_PLANE);
//   // proj.setIndices (inliers);
//   proj.setInputCloud (inlier_cloud);
//   pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
//   coefficients->values.resize (4);
//   coefficients->values[0] = coeffs[0]; 
//   coefficients->values[1] = coeffs[1];
//   coefficients->values[2] = coeffs[2];
//   coefficients->values[3] = coeffs[3];
//   proj.setModelCoefficients (coefficients);
//   proj.filter (*cloud_projected);

//   PointCloud::Ptr output_cloud(new PointCloud);

//   // DilatePlane(cloud_projected, coeffs, output_cloud);
//  // pcl::io::savePCDFileBinary("regions" + to_string(i) + ".pcd", *cloud_projected);

//   // Dial
//   *all_cloud += *cloud_projected;
  
//  }


//   // FilterPCLWithSor(all_cloud, filtered, 20, 1.0);
  
  // pcl::io::savePCDFileBinary("all_floor_pcl.pcd", *all_floor_pcl);
  // pcl::io::savePCDFileBinary("all_dilate.pcd", *all_dilate);
  if (param_.use_upsample_filtering)
        all_floor_pcl  = UpsamplingPointCloud(all_floor_pcl);

   *output_pcl = *all_floor_pcl;
  //  pcl::io::savePCDFileBinary("output.pcd", *output_pcl);
// *output_pcl += *all_dilate;
  // pcl::io::savePCDFileBinary("output_sum.pcd", *output_pcl);

  output_pcl->header = input_pcl->header;

  return 0;
}

PointCloud::Ptr FloorExtractor::normal_filtering(const PointCloud::Ptr &cloud) const
{
  pcl::NormalEstimation<PointType, pcl::Normal> ne;
  ne.setInputCloud(cloud);

  pcl::search::KdTree<PointType>::Ptr tree(new pcl::search::KdTree<PointType>);
  ne.setSearchMethod(tree);

  pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
 ne.setKSearch(50);
   // ne.setRadiusSearch(0.2);

  ne.setViewPoint(0.0f, 0.0f, param_.sensor_height);
  ne.compute(*normals);

  PointCloud::Ptr filtered(new PointCloud);
  filtered->reserve(cloud->size());

  for (int i = 0; i < cloud->size(); i++)
  {
    float dot = normals->at(i).getNormalVector3fMap().normalized().dot(Eigen::Vector3f::UnitZ());
    if (std::abs(dot) > std::cos(param_.normal_filter_thresh * M_PI / 180.0))
    {
      filtered->push_back(cloud->at(i));
    }
  }

  filtered->width = filtered->size();
  filtered->height = 1;
  filtered->is_dense = false;
  
  return filtered;
}


PointCloud::Ptr   FloorExtractor::FilterFloor(PointCloud::Ptr input_pcl, PointCloud::Ptr non_floor_pcl) {
  PointCloud::Ptr floor_pcl (new PointCloud());
  pcl::SampleConsensusModelPlane<PointType>::Ptr model_p(
      new pcl::SampleConsensusModelPlane<PointType>(input_pcl));
  pcl::RandomSampleConsensus<PointType> ransac(model_p);
  ransac.setDistanceThreshold(0.15);
  ransac.computeModel();
 
  pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
  ransac.getInliers(inliers->indices);

  pcl::ExtractIndices<PointType> extract;
  extract.setInputCloud(input_pcl);
  extract.setIndices(inliers);
  extract.setNegative (true);
  extract.filter (*non_floor_pcl);

  Eigen::Matrix4f tilt_matrix = Eigen::Matrix4f::Identity();
  tilt_matrix.topLeftCorner(3, 3) = Eigen::AngleAxisf(param_.tilt_deg * M_PI / 180.0f, Eigen::Vector3f::UnitY()).toRotationMatrix();
  // verticality check of the detected floor's normal
  Eigen::Vector4f reference = tilt_matrix.inverse() * Eigen::Vector4f::UnitZ();

  Eigen::VectorXf coeffs;
  ransac.getModelCoefficients(coeffs);

  double dot = coeffs.head<3>().dot(reference.head<3>());
  if (std::abs(dot) < std::cos(param_.floor_normal_thresh * M_PI / 180.0)) {

   return floor_pcl;
  }


  PointCloud::Ptr cloud_projected(new PointCloud);
  PointCloud::Ptr inlier_cloud(new PointCloud);

  extract.setNegative (false);
  extract.filter(*inlier_cloud);


  // pcl::ProjectInliers<PointType> proj;
  // proj.setModelType (pcl::SACMODEL_PLANE);
  // // proj.setIndices (inliers);
  // proj.setInputCloud (inlier_cloud);
  // pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
  // coefficients->values.resize (4);
  // coefficients->values[0] = coeffs[0]; 
  // coefficients->values[1] = coeffs[1];
  // coefficients->values[2] = coeffs[2];
  // coefficients->values[3] = coeffs[3];
  // proj.setModelCoefficients (coefficients);
  // proj.filter (*cloud_projected);

  FilterPCLWithSor(inlier_cloud, floor_pcl, 30, 1.0);

  return floor_pcl;
}

// std::vector <PointCloud::Ptr> FloorExtractor::SegPointcloud(const PointCloud::Ptr &cloud)
// {
//   static int first_threshold_value_  = 100;
//   std::vector <pcl::PointIndices> clusters_;
//   std::vector <PointCloud::Ptr> segmented_pcls;

//   pcl::NormalEstimation<PointType, pcl::Normal> ne;
//   ne.setInputCloud(cloud);

//   pcl::search::KdTree<PointType>::Ptr tree(new pcl::search::KdTree<PointType>);
//   ne.setSearchMethod(tree);

//   pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
//   ne.setKSearch(10);
//   ne.setViewPoint(0.0f, 0.0f, param_.sensor_height);
//   ne.compute(*normals);
//   int  min_cluster_size_(50);
//   int   max_cluster_size_(1000000);
//   int   number_of_neighbours_(30);
//   float     smoothness_threshold_(3.0);
//   float    curvature_threshold_(1.0);

//   pcl::RegionGrowing<PointType, pcl::Normal> reg;
//   reg.setMinClusterSize (min_cluster_size_);
//   reg.setMaxClusterSize (max_cluster_size_);
//   reg.setSearchMethod (tree);
//   reg.setNumberOfNeighbours (number_of_neighbours_);
//   reg.setInputCloud (cloud);
//   reg.setInputNormals (normals);
//   reg.setSmoothnessThreshold (smoothness_threshold_ / 180.0 * M_PI);
//   reg.setCurvatureThreshold (curvature_threshold_);

//   reg.extract (clusters_);
//   std::cout << "Number of clusters_ is equal to " << clusters_.size() << std::endl;
//   std::cout << "First cluster has " << clusters_[0].indices.size() << " points." << endl;
// 	std::cout << "Last cluster has " << clusters_[clusters_.size()-1].indices.size() << " points." << endl;
//   // auto colored_cloud = reg.getColoredCloud();
//   // pcl::io::savePCDFileBinary("colored_region.pcd", *colored_cloud);
//   for(size_t index = 0; index < clusters_.size(); index++){
// 		if(clusters_[index].indices.size() >= first_threshold_value_){
//         PointCloud::Ptr segment_pcl(new PointCloud);

//         pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
//         inliers->indices = move(clusters_[index].indices);
//         pcl::ExtractIndices<PointType> extract;
//         extract.setInputCloud (cloud);
//         extract.setIndices (inliers);
//         extract.setNegative (false);
//         extract.filter (*segment_pcl);

//         segmented_pcls.push_back(segment_pcl);
//     }
//   }

//   return segmented_pcls;
// }
// int FloorExtractor::DilatePlane(PointCloud::Ptr input_pcl,  Eigen::VectorXf plane_coefficients, PointCloud::Ptr output_pcl) {
//   Eigen::Vector4f minPt;
//    Eigen::Vector4f maxPt;
//   pcl::getMinMax3D(*input_pcl, minPt, maxPt);

//   vector<PointType> points_to_fill;

//   pcl::KdTreeFLANN<PointType>::Ptr kdtreeGlobalMap(new pcl::KdTreeFLANN<PointType>());
//   std::vector<int> pointSearchIndLoop;
//   std::vector<float> pointSearchSqDisLoop;
//   kdtreeGlobalMap->setInputCloud(input_pcl);
//   float step  = 0.1f;
//   for (float x = minPt[0] - step; x < maxPt[0] + step; x += step) {
//     for (float y = minPt[1] - step; y < maxPt[1] + step; y += step) {
//       float z = (plane_coefficients[0] * x +  plane_coefficients[1] * y +  plane_coefficients[3]) / (-plane_coefficients[2]);
//       PointType point;
//       point.x = x, point.y = y, point.z = z;
//       int n = kdtreeGlobalMap->nearestKSearch(point, 1, pointSearchIndLoop, pointSearchSqDisLoop);
//       if ( n == 0)
//         continue;
//       if (pointSearchSqDisLoop[0] > step / 2 && pointSearchSqDisLoop[0] < step ) {
//         points_to_fill.push_back(point);
//       }

//     }

//   float fill_step  = 0.04f;
//     for (const auto & point_to_fill : points_to_fill) {
//         for (float x = point_to_fill.x - step  ; x < point_to_fill.x + step  ; x += fill_step) {
//             for (float y = point_to_fill.y - step ; y < point_to_fill.y + step ; y += fill_step) {
//               float z = (plane_coefficients[0] * x +  plane_coefficients[1] * y +  plane_coefficients[3]) / (-plane_coefficients[2]);
//               PointType new_point;
//               new_point.x = x, new_point.y  = y, new_point.z = z;
//               output_pcl->push_back(new_point);
//           }
//         }
//     }
    
//   }
//   // kdtreeGlobalMap->radiusSearch(copy_cloudKeyPoses3D->back(), historyKeyframeSearchRadius, pointSearchIndLoop, pointSearchSqDisLoop, 0);
// return 0;
// }
}
}  // namespace object_location
