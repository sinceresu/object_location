#pragma once

#include <memory>

// #include "person_localize2d/person_localize2d_interface.h"
#include "person_localize2d/person_localize2d_interface.h"

namespace person_localize2d{

class Person2dLocalizer : public PersonLocalize2dInterface
{
public:
  Person2dLocalizer();
  ~Person2dLocalizer(){};
  virtual int  Localize(const std::vector<cv::Point2i> & persion_joints,  const cv::Rect & person_box, cv::Point2i& position) override;

private:


  bool initialized_ = false;
};
} // namespace person_localize2d

