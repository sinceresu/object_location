#pragma once
#include <string>
#include <vector>
#include <set>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <opencv2/opencv.hpp>

#include "cam_param_reader.h"

namespace object_location
{
  namespace object_localize3d
  {

    class RotateCamParamReader : public CamParamReader
    {
    public:
      explicit RotateCamParamReader();
      virtual ~RotateCamParamReader();

      RotateCamParamReader(const RotateCamParamReader &) = delete;
      RotateCamParamReader &operator=(const RotateCamParamReader &) = delete;

      virtual int SetRotations(const std::vector<double> &_rads) override;

    private:
      virtual int LoadExtrinsic(cv::FileStorage _fs) override;

      std::vector<Eigen::Matrix4d> axis_mats_;
      Eigen::Matrix4d tool_mat_;
    };

  }
}
