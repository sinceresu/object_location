#ifndef __COMMON_H__
#define __COMMON_H__

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

using namespace std;
namespace   object_location {
namespace common {
  typedef pcl::PointXYZ PointType;
  typedef pcl::PointCloud<PointType> PointCloud;
  typedef pcl::PointNormal PointNormalT;
}
}

#endif