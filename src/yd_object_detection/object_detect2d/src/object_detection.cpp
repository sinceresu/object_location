/**************************************************************************

Copyright: YiDa

Version: 1.00

Author: LiXingren

Date: 2022-05-18

Description: Input image from rtsp stream node. Output object detection results.
             2022-05-18: First edited.

**************************************************************************/
#include <iostream>
#include <exception>
#include "opencv2/opencv.hpp"
#include "ros/ros.h"
#include "object_detect2d_msgs/ObjectDetect2d.h"
#include <cv_bridge/cv_bridge.h>
#include "class_detector.h"
#include "BYTETracker.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/daily_file_sink.h"

#define __app_name__ "object_detection"

std::string strCameraId;
int uModelPersonId;
std::vector<uint16_t> viOutputTypeArray;
double dModelWidthExtendScale, dModelHeightExtendScale, dHWRatio;
double dModelThresh;
int iTrackFps = 25;
double dRosRate = 100.0;
bool bTestMode = false;
bool bIsShowImage = false;
ros::Subscriber subVisibleStream;
ros::Publisher pubDetectResult;
std::unique_ptr<Detector> detector(new Detector());
BYTETracker tracker;
int iFirst = 0;

cv::Mat cvLastImageGray;
std::vector<std::vector<cv::Point2f>> lastAllPoints;

void Stringsplit(const std::string &str, const char split, std::vector<std::string> &res)
{
    if (str == "")
        return;
    //在字符串末尾也加入分隔符，方便截取最后一段
    std::string strs = str + split;
    size_t pos = strs.find(split);

    // 若找不到内容则字符串搜索函数返回 npos
    while (pos != strs.npos)
    {
        std::string temp = strs.substr(0, pos);
        res.push_back(temp);
        //去掉已分割的字符串,在剩下的字符串中进行分割
        strs = strs.substr(pos + 1, strs.size());
        pos = strs.find(split);
    }
}

void Sleep(int ms)
{
    struct timeval delay;
    delay.tv_sec = 0;
    delay.tv_usec = ms * 1000;
    select(0, NULL, NULL, NULL, &delay);
}

BatchResult Detect_yolo(cv::Mat image)
{
    std::vector<BatchResult> batch_res;

    std::vector<cv::Mat> batch_img;
    cv::Mat temp = image.clone();
    batch_img.push_back(temp);
    detector->detect(batch_img, batch_res);
    return batch_res[0];
}

std::vector<Result> get_person_id(BatchResult &batchResult)
{
    std::vector<Result> outputs;
    std::vector<cv::Rect> rects;
    std::vector<float> probs;
    for (const auto &r : batchResult)
    {
        if (r.id == uModelPersonId)
        {
            rects.push_back(r.rect);
            probs.push_back(r.prob);
        }
    }
    int iInputCnt = rects.size();
    vector<STrack> output_stracks = tracker.update(rects, probs);
    int iOutputCnt = output_stracks.size();
    if (iInputCnt != iOutputCnt)
    {
        spdlog::error("iInputCnt != iOutputCnt,iInputCnt:{},iOutputCnt:{}", iInputCnt, iOutputCnt);
    }
    for (int i = 0; i < output_stracks.size(); i++)
    {
        vector<float> tlwh = output_stracks[i].tlwh;
        bool vertical = tlwh[2] / tlwh[3] > 1.6;
        if (tlwh[2] * tlwh[3] > 20 && !vertical)
        {
            Result result;
            result.id = output_stracks[i].track_id;
            result.prob = output_stracks[i].score;
            result.rect = cv::Rect(tlwh[0], tlwh[1], tlwh[2], tlwh[3]);
            outputs.push_back(result);
        }
    }
    return outputs;
}

void visible_stream_callback(const sensor_msgs::Image &rosImage)
{
    try
    {
        spdlog::info("receive image");
        struct timeval tstart, tend;
        double timeUsed;
        gettimeofday(&tstart, NULL);

        cv_bridge::CvImagePtr cv_ptr;
        try
        {
            cv_ptr = cv_bridge::toCvCopy(rosImage, sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception &e)
        {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }
        catch (std::exception &e)
        {
            std::cout << "Standard exception: " << e.what() << std::endl;
            return;
        }
        cv::Mat cvImage = cv_ptr->image;
        if (cvImage.rows == 0)
        {
            spdlog::info("image null,continue");
            cvImage.release();
            return;
        }
        std::string strWindowsName = __app_name__;
        if (bIsShowImage)
        {
            cv::namedWindow(strWindowsName, cv::WINDOW_NORMAL);
            cv::resizeWindow(strWindowsName, 640, 480);
        }

        gettimeofday(&tend, NULL);
        timeUsed = (tend.tv_sec - tstart.tv_sec) + (tend.tv_usec - tstart.tv_usec) * 0.000001;
        std::cout << "transform cv_bridge to cvmat cost time(s): " << timeUsed << std::endl;

        struct timeval t1, t2;
        gettimeofday(&t1, NULL);
        BatchResult batchResult = Detect_yolo(cvImage);
        gettimeofday(&t2, NULL);
        double timeuse = (t2.tv_sec - t1.tv_sec) + (t2.tv_usec - t1.tv_usec) * 0.000001;
        spdlog::info("yolo detection cost time(s):{}", timeuse); //输出时间（单位：uｓ）

        if (batchResult.size() <= 0)
        {
            if (bIsShowImage)
            {
                cv::imshow(strWindowsName, cvImage);
                cv::waitKey(1); //（单位：mｓ）
            }
            // cvImage.release();
            return;
        }

        object_detect2d_msgs::ObjectDetect2d objectDetect2d;
        objectDetect2d.cam_id = strCameraId;
        uint16_t uOtherId = 0;
        // cv::Mat cvImageGray;
        // cv::cvtColor(cvImage, cvImageGray, CV_RGB2GRAY);

        std::vector<Result> output_stracks = get_person_id(batchResult);

        for (const auto &r : output_stracks)
        {
            cv::Rect rectObject;

            objectDetect2d.object_ids.push_back(r.id - 1);

            int x, y, height, width;
            double dV = double(r.rect.height / r.rect.width) - dHWRatio;
            if (dV >= 0)
            {
                x = r.rect.x - r.rect.height / (2 * dHWRatio);
                x = x >= 0 ? x : 0;
                int x2 = r.rect.x + r.rect.height / (2 * dHWRatio);
                x2 = x2 <= cvImage.cols ? x2 : cvImage.cols;
                width = x2 - x;
            }
            else
            {
                x = r.rect.x;
                width = r.rect.width;
            }
            y = r.rect.y;
            height = r.rect.height;

            int iModelWidthExtendScale = int(width * dModelWidthExtendScale);
            int iModelHeightExtendScale = int(height * dModelHeightExtendScale);
            rectObject.x = x - iModelWidthExtendScale >= 0 ? x - iModelWidthExtendScale : 0;
            rectObject.y = y - iModelWidthExtendScale >= 0 ? y - iModelHeightExtendScale : 0;
            int iRx = x + width + iModelWidthExtendScale <= cvImage.cols ? x + width + iModelWidthExtendScale : cvImage.cols;
            int iRy = y + height + iModelHeightExtendScale <= cvImage.rows ? y + height + iModelHeightExtendScale : cvImage.rows;
            rectObject.width = iRx - rectObject.x;
            rectObject.height = iRy - rectObject.y;

            spdlog::info("r.rect,x:{},y:{},width:{},height:{}", r.rect.x, r.rect.y, r.rect.width, r.rect.height);
            spdlog::info("rect,x:{},y:{},width:{},height:{}", rectObject.x, rectObject.y, rectObject.width, rectObject.height);

            // std::vector<cv::Point2f> point2;
            // cv::goodFeaturesToTrack(cvImageGray(r.rect), point2, 100, 0.01, 10, cv::Mat());
            // for (int i = 0; i < point2.size(); i++)
            // {
            //     point2[i].x += r.rect.x;
            //     point2[i].y += r.rect.y;
            // }
            // if (cvLastImageGray.data)
            // {
            //     std::vector<uchar> status;
            //     std::vector<float> err;

            //     // cv::Point2f cvLTPoint;
            //     // cvLTPoint.x = r.rect.x + r.rect.width / 3;
            //     // cvLTPoint.y = r.rect.y + r.rect.height / 3;
            //     // lastPoints.push_back(cvLTPoint);
            //     // cv::Point2f cvLBPoint;
            //     // cvLBPoint.x = r.rect.x + r.rect.width / 3;
            //     // cvLBPoint.y = r.rect.y + r.rect.height * 2 / 3;
            //     // lastPoints.push_back(cvLBPoint);
            //     // cv::Point2f cvRTPoint;
            //     // cvRTPoint.x = r.rect.x + r.rect.width * 2 / 3;
            //     // cvRTPoint.y = r.rect.y + r.rect.height / 3;
            //     // lastPoints.push_back(cvRTPoint);
            //     // cv::Point2f cvRBPoint;
            //     // cvRBPoint.x = r.rect.x + r.rect.width * 2 / 3;
            //     // cvRBPoint.y = r.rect.y + r.rect.height * 2 / 3;
            //     // lastPoints.push_back(cvRBPoint);
            //     // calcOpticalFlowPyrLK(cvLastImageGray, cvImageGray, lastPoints, point2, status, err, cv::Size(50, 50), 3); // LK金字塔
            //     // float fOutAngle = 0;
            //     // for (int i = 0; i < point2.size(); i++)
            //     // {
            //     //     line(cvImage, lastPoints[i], point2[i], cv::Scalar(255, 0, 0), 2);
            //     //     float fAngle = cvFastArctan(point2[i].y - lastPoints[i].y, point2[i].x - lastPoints[i].x);
            //     //     fOutAngle += fAngle;
            //     //     spdlog::info("fAngle:{}", fAngle);
            //     // }
            //     // fOutAngle = fOutAngle / point2.size();
            //     // spdlog::info("fOutAngle:{}", fOutAngle);
            // }

            objectDetect2d.object_types.push_back(viOutputTypeArray[uModelPersonId]);

            geometry_msgs::Polygon polygon;
            geometry_msgs::Point32 lPoint, rPoint;
            lPoint.x = rectObject.x;
            lPoint.y = rectObject.y;
            rPoint.x = rectObject.x + rectObject.width;
            rPoint.y = rectObject.y + rectObject.height;
            polygon.points.push_back(lPoint);
            polygon.points.push_back(rPoint);
            objectDetect2d.object_boxes.push_back(polygon);

            cv::Mat cvCutImage = cvImage(rectObject);
            cv_bridge::CvImage cvi;
            ros::Time time = ros::Time::now();
            cvi.header.stamp = time;
            cvi.header.frame_id = "image";
            cvi.encoding = "bgr8";
            cvCutImage.copyTo(cvi.image);
            sensor_msgs::Image im;
            cvi.toImageMsg(im);
            objectDetect2d.object_images.push_back(im);

            if (bIsShowImage)
            {
                rectangle(cvImage, r.rect, cv::Scalar(0, 0, 255), 2, 8, 0);
                int y = r.rect.y - 5 ? r.rect.y - 5 > 0 : 0;
                putText(cvImage, format("%d", r.id), Point(r.rect.x, y),
                        0, 2.6, Scalar(0, 0, 255), 2, LINE_AA);
            }
        }

        for (const auto &r : batchResult)
        {
            if (r.id >= viOutputTypeArray.size())
            {
                continue;
            }
            if (r.id == uModelPersonId)
            {
                continue;
            }

            if (r.prob < dModelThresh)
            {
                continue;
            }

            cv::Rect rectObject;
            objectDetect2d.object_ids.push_back(uOtherId);
            uOtherId++;
            rectObject = r.rect;

            objectDetect2d.object_types.push_back(viOutputTypeArray[r.id]);

            geometry_msgs::Polygon polygon;
            geometry_msgs::Point32 lPoint, rPoint;
            lPoint.x = rectObject.x;
            lPoint.y = rectObject.y;
            rPoint.x = rectObject.x + rectObject.width;
            rPoint.y = rectObject.y + rectObject.height;
            polygon.points.push_back(lPoint);
            polygon.points.push_back(rPoint);
            objectDetect2d.object_boxes.push_back(polygon);

            // cv::Mat cvCutImage = cvImage(rectObject);
            // cv_bridge::CvImage cvi;
            // ros::Time time = ros::Time::now();
            // cvi.header.stamp = time;
            // cvi.header.frame_id = "image";
            // cvi.encoding = "bgr8";
            // cvCutImage.copyTo(cvi.image);
            // sensor_msgs::Image im;
            // cvi.toImageMsg(im);
            // objectDetect2d.object_images.push_back(im);

            if (bIsShowImage)
            {
                rectangle(cvImage, rectObject, cv::Scalar(0, 0, 255), 2, 8, 0);
            }
        }

        std_msgs::Header header;
        header.stamp = ros::Time::now();
        objectDetect2d.header = header;

        if (output_stracks.size() > 0 && bTestMode)
        {
            std::string strOutputImagePath = "/home/lxr/workdir/src/yd_person_keypoints/person_pose2d/database/" +
                                             std::to_string(header.stamp.sec) + "_" + std::to_string(header.stamp.nsec) + ".jpg";
            cv::imwrite(strOutputImagePath, cvImage);
        }
        pubDetectResult.publish(objectDetect2d);
        if (bIsShowImage)
        {
            cv::imshow(strWindowsName, cvImage);
            cv::waitKey(1); //（单位：mｓ）
            cvImage.release();
        }

        gettimeofday(&tend, NULL);
        timeUsed = (tend.tv_sec - tstart.tv_sec) + (tend.tv_usec - tstart.tv_usec) * 0.000001;
        std::cout << "Object detection cost time(s): " << timeUsed << std::endl;

        // cvLastImageGray = cvImageGray;
        // lastPoints = point2;
        // cv::cvtColor(cvImage, cvLastImageGray, CV_RGB2GRAY);
    }
    catch (std::exception &e)
    {
        spdlog::error("Standard exception:{}", e.what());
    }
}

// int optical_flow()
// {
//     cv::Mat image1, image2;
//     std::vector<cv::Point2f> point1, point2, pointCopy;
//     std::vector<uchar> status;
//     std::vector<float> err;

//     cv::VideoCapture video(0);
//     video >> image1;
//     cv::Mat image1Gray, image2Gray;
//     cv::cvtColor(image1, image1Gray, CV_RGB2GRAY);
//     goodFeaturesToTrack(image1Gray, point1, 100, 0.01, 10, cv::Mat());
//     pointCopy = point1;
//     for (int i = 0; i < point1.size(); i++)    //绘制特征点位
//     {
//         circle(image1, point1[i], 1, cv::Scalar(0, 0, 255), 2);
//     }
//     cv::namedWindow("光流特征图");
//     while (true)
//     {
//         video >> image2;
//         if (cv::waitKey(33) == ' ')  //按下空格选择当前画面作为标定图像
//         {
//             cvtColor(image2, image1Gray, CV_RGB2GRAY);
//             goodFeaturesToTrack(image1Gray, point1, 100, 0.01, 10, cv::Mat());
//             pointCopy = point1;
//         }
//         cvtColor(image2, image2Gray, CV_RGB2GRAY);
//         calcOpticalFlowPyrLK(image1Gray, image2Gray, point1, point2, status, err, cv::Size(50, 50), 3); //LK金字塔
//         int tr_num = 0;
//         std::vector<unsigned char>::iterator status_itr = status.begin();
//         while (status_itr != status.end()) {
//             if (*status_itr > 0)
//                 tr_num++;
//             status_itr++;
//         }
//         if (tr_num < 6) {
//             std::cout << "you need to change the feat-img because the background-img was all changed" << std::endl;
//             if (cv::waitKey(0) == ' ') {
//                 cvtColor(image2, image1Gray, CV_RGB2GRAY);
//                 goodFeaturesToTrack(image1Gray, point1, 100, 0.01, 10, cv::Mat());
//                 pointCopy = point1;
//             }
//         }
//         for (int i = 0; i < point2.size(); i++)
//         {
//             circle(image2, point2[i], 1, cv::Scalar(0, 0, 255), 2);
//             line(image2, pointCopy[i], point2[i], cv::Scalar(255, 0, 0), 2);
//         }

//         imshow("光流特征图", image2);
//         swap(point1, point2);
//         image1Gray = image2Gray.clone();
//     }

//     return 0;
// }

void Init()
{
    try
    {
        // auto daily_logger = spdlog::daily_logger_mt(__app_name__, "log/daily.txt", 0, 0);
        spdlog::set_level(spdlog::level::info);
        // spdlog::set_default_logger(daily_logger);

        ros::NodeHandle node_handle;
        std::string strVisibleStreamTopic, strDetectResultTopic, strHeartbeat, strModelDir, strOutputTypeArray, strModelThresh, strModelPersonId;
        ros::param::get("test_mode", bTestMode);

        if (bTestMode)
        {
            bIsShowImage = true;
        }
        else
        {
            ros::param::get("is_show_image", bIsShowImage);
        }

        ros::param::get("visible_stream_topic", strVisibleStreamTopic);
        std::cout << "strVisibleStreamTopic:" << strVisibleStreamTopic << std::endl;
        ros::param::get("detect_result_topic", strDetectResultTopic);
        ros::param::get("heartbeat_topic", strHeartbeat);
        ros::param::get("model_dir", strModelDir);
        ros::param::get("camera_id", strCameraId);
        ros::param::get("output_type_array", strOutputTypeArray);
        ros::param::get("model_person_id", uModelPersonId);

        ros::param::get("model_width_extend_scale", dModelWidthExtendScale);
        ros::param::get("model_height_extend_scale", dModelHeightExtendScale);
        ros::param::get("height_width_ratio", dHWRatio);
        ros::param::get("model_thresh", dModelThresh);
        ros::param::get("track_fps", iTrackFps);
        ros::param::get("ros_rate", dRosRate);

        std::vector<std::string> vstrOutputTypeArray;
        Stringsplit(strOutputTypeArray, ',', vstrOutputTypeArray);
        int iOutputTypeCnt = vstrOutputTypeArray.size();
        for (int i = 0; i < iOutputTypeCnt; ++i)
        {
            uint16_t iOutputType = stoi(vstrOutputTypeArray[i]);
            viOutputTypeArray.push_back(iOutputType);
        }

        spdlog::info("strVisibleStreamTopic:{}", strVisibleStreamTopic);
        subVisibleStream = node_handle.subscribe(strVisibleStreamTopic, 1, &visible_stream_callback);
        pubDetectResult = node_handle.advertise<object_detect2d_msgs::ObjectDetect2d>(strDetectResultTopic, 1);

        Config config_v4_tiny;
        config_v4_tiny.net_type = YOLOV4_TINY;
        config_v4_tiny.detect_thresh = 0.01;
        config_v4_tiny.file_model_cfg = strModelDir + "/yolov4-tiny.cfg";
        config_v4_tiny.file_model_weights = strModelDir + "/yolov4-tiny.weights";
        config_v4_tiny.calibration_image_list_file_txt = strModelDir + "/calibration_images.txt";
        int iLastPos = strModelDir.find_last_of("/");
        std::string strCmd = "cd " + strModelDir + " && find " + strModelDir.substr(0, iLastPos) + "/calib_images/ -name '*.jpg' > calibration_images.txt";
        system(strCmd.data());

        config_v4_tiny.inference_precison = INT8;
        detector->init(config_v4_tiny);

        tracker.init(iTrackFps, 30);
    }
    catch (std::exception &e)
    {
        spdlog::error("Standard exception:{}", e.what());
    }
}

int main(int argc, char **argv)
{
    try
    {
        ros::init(argc, argv, __app_name__);
        ros::Time::init();

        Init();

        ros::Rate rate(dRosRate);

        while (ros::ok())
        {
            ros::spinOnce();
            rate.sleep();
        }
    }
    catch (std::exception &e)
    {
        spdlog::error("Standard exception:{}", e.what());
    }

    spdlog::shutdown();

    return 0;
}
