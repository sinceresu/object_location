/**************************************************************************

Copyright: YiDa

Version: 1.00

Author: LiXingren

Date: 2022-05-18

Description: Input image from rtsp stream node. Output object detection results.
             2022-05-18: First edited.

**************************************************************************/

#include <iostream>
#include <sys/time.h>
// #include <string>


#include "time.h"
#include "ros/ros.h"

#include "object_detect2d_msgs/ObjectDetect2d.h"
#include "class_detector.h"

#define __app_name__ "object_detection"

namespace ObjectDetectionNode
{
    class ObjectDetection
    {
        

        public:
        ObjectDetection();
        ~ObjectDetection();

        std::string strCameraId;
        int uModelPersonId;
        std::vector<uint16_t> viOutputTypeArray;
        double dModelWidthExtendScale, dModelHightExtendScale;
        bool bTestMode = true;
        bool bIsShowImage = true;
        ros::Publisher pubDetectResult;

        void Init();
        void Sleep(int ms);
        BatchResult Detect_yolo(cv::Mat image);
        void visible_stream_callback(const sensor_msgs::Image &rosImage);
        void Stringsplit(const std::string &str, const char split, std::vector<std::string> &res);

        Detector detector;
        // private:
        
    };
}
