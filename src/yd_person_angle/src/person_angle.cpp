/**************************************************************************

Copyright: YiDa

Version: 1.10

Author: LiXingren

Date: 2022-05-18

Description: Input person keypoints.
             Output angle vertical with the surface of photo.
             2022-05-18: First edited.

**************************************************************************/
#include <iostream>
#include <exception>
#include <vector>
#include <stdint.h>
#include <math.h>
#include <opencv2/opencv.hpp>


#include "person_angle/person_angle.h"
#include "common/person_joints.h"


using namespace cv;
using namespace std;
using namespace object_location::common;

namespace PersonAngleLib
{
float kMaxParallelAngle = 25.f;
namespace {
    bool GetBodyPoints(const vector<Point2i>& person_joints, vector<Point2f>& joints_2d, vector<Point3f>& joints_3d) {
        joints_2d.clear();
        joints_3d.clear();
        auto person_joint = person_joints[(int)RawJointOrder::LEFT_SHOULDER];
        if (person_joint.x != 0) {
            joints_2d.push_back(person_joint);
            joints_3d.push_back(Point3f(-0.22f,  -0.33f, 0.f));
        }
        person_joint = person_joints[(int)RawJointOrder::NECK];
        if (person_joint.x != 0) {
            joints_2d.push_back(person_joint);
            joints_3d.push_back(Point3f(0.f,  -0.33, 0.f));
        }
        person_joint = person_joints[(int)RawJointOrder::RIGHT_SHOULDER];
        if (person_joint.x != 0) {
            joints_2d.push_back(person_joint);
            joints_3d.push_back(Point3f(0.22f,  -0.33, 0.f));
        }

        person_joint = person_joints[(int)RawJointOrder::LEFT_WAIST];
        if (person_joint.x != 0) {
            joints_2d.push_back(person_joint);
            joints_3d.push_back(Point3f(-0.12f,  0.33, 0.f));
        }

        person_joint = person_joints[(int)RawJointOrder::RIGHT_WAIST];
        if (person_joint.x != 0) {
            joints_2d.push_back(person_joint);
            joints_3d.push_back(Point3f(0.12f,  0.33, 0.f));
        }

        //shoulder and waist finded;
        if (joints_2d.size() >= 4) 
            return true;

        //add head;
        person_joint = person_joints[(int)RawJointOrder::HEAD];
        if (person_joint.x != 0) {
            joints_2d.push_back(person_joint);
            joints_3d.push_back(Point3f(0.f,  -0.51, 0.f));

        }

        return joints_2d.size() >= 4;
    }


bool IsParallelToCamera( const cv::Vec3f& orientation){
    cv::Vec3f camera_normal(0.f, 0.f, 1.0f);
    float angle = acosf(camera_normal.dot(orientation)) * 180 / M_PI ;
    return angle > 90.f - kMaxParallelAngle && angle < 90.f + kMaxParallelAngle;
}

bool GetOrientationByToes(const std::vector<cv::Point2i>& person_joints,  Vec2f& toe_orientation){

    if ( (person_joints[(int)RawJointOrder::LEFT_TOE].x == 0 || person_joints[(int)RawJointOrder::LEFT_HEEL].x == 0) && 
        (person_joints[(int)RawJointOrder::RIGHT_TOE].x == 0 || person_joints[(int)RawJointOrder::RIGHT_HEEL].x == 0))
        return false;

    toe_orientation = Vec2f(0.f, 0.f);
    if (person_joints[(int)RawJointOrder::LEFT_TOE].x != 0 && person_joints[(int)RawJointOrder::LEFT_HEEL].x != 0)  {
        auto toe_vec = person_joints[(int)RawJointOrder::LEFT_TOE] - person_joints[(int)RawJointOrder::LEFT_HEEL];
        toe_orientation = Vec2f(toe_vec.x, toe_vec.y);
    }
    if (person_joints[(int)RawJointOrder::RIGHT_TOE].x != 0 && person_joints[(int)RawJointOrder::RIGHT_HEEL].x != 0)  {
        auto toe_vec = person_joints[(int)RawJointOrder::RIGHT_TOE] - person_joints[(int)RawJointOrder::RIGHT_HEEL];
        toe_orientation += Vec2f(toe_vec.x, toe_vec.y);
    }

    // toe_orientation = normalize(toe_orientation);

    return true;
}

Vec3f CorrectOrientationByToeOrientation(const Vec2f& toe_orientation,  const cv::Vec3f& orientation){
    Vec2f orientation_2d(orientation[0], orientation[1]);
    auto angle = orientation_2d.dot(toe_orientation);
    return angle < 0.f ? Vec3f(-orientation[0], orientation[1], -orientation[2]): orientation;
}

Vec3f CorrectOrientationByToes(const std::vector<cv::Point2i>& person_joints,  const cv::Vec3f& orientation){
    Vec2f toe_orientation(0.f, 0.f);
    if (!GetOrientationByToes(person_joints, toe_orientation))
        return orientation;

    return CorrectOrientationByToeOrientation(toe_orientation,orientation );
}
}


PersonAngle::PersonAngle(/* args */)
{
    std::cout << "constructor" << std::endl;
}

PersonAngle::~PersonAngle()
{
    std::cout << "destructor" << std::endl;
}


int PersonAngle::LoadCalibrationFile(const std::string& calibration_filepath)
  {
    cv::FileStorage fs;

    fs.open(calibration_filepath, cv::FileStorage::READ);
        if (!fs.isOpened())
    {
        return -1;
    }

    fs["CameraMat"] >> camera_intrinsic_;
    fs["DistCoeff"] >> camera_distortion_;

    return 0;
}

bool PersonAngle:: ComputeOrientation(const std::vector<cv::Point2i>& person_joints, Vec3f & person_orientation){
    person_orientation = Vec3f(0, 0, 0);
    vector<Point2f> image_points;
    vector<Point3f> object_points;

    if (!GetBodyPoints(person_joints, image_points, object_points))
        return false;

    vector<Point2f> object_points_planar;
    for (size_t i = 0; i < object_points.size(); i++)
    {
        object_points_planar.push_back(Point2f(object_points[i].x, object_points[i].y));
    }
    Mat rvec, tvec;
    solvePnP(object_points, image_points, camera_intrinsic_, camera_distortion_, rvec, tvec);

    Mat R;
    Rodrigues(rvec, R);

    Mat normal = (Mat_<double>(3,1) << 0, 0, 1);
    normal =  R*normal;
    // cout << R << endl;
    person_orientation = normal;

    if (IsParallelToCamera(person_orientation)) {
        person_orientation =  CorrectOrientationByToes(person_joints,  person_orientation);
    }

    return true;
}


 bool   PersonAngle::Compute2DOrientation(const std::vector<cv::Point2i>& vKeypoints, cv::Vec2f & person_orientation) {
    return GetOrientationByToes(vKeypoints, person_orientation);
 }
}