#include "object_localizer_factory.h"

#include "object_localizer.h"
#include "person_localizer.h"

using namespace std;

namespace object_location{
namespace object_localize3d{

std::shared_ptr<ObjectLocalizer> ObjectLocalizerFactory::CreateObjectLocalizer(std::string object_label)  {
  if (object_label == "person")
    return std::make_shared<PersonLocalizer>();
  return std::make_shared<ObjectLocalizer>();
}
} // namespace object_location
}// namespace object_location
