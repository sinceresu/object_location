#ifndef _localize_person2d_TIMED_IMAGE_H
#define _localize_person2d_TIMED_IMAGE_H

#include "opencv2/opencv.hpp"

namespace person_localize2d{
cv::Vec3b getColorSubpix(const cv::Mat& img, cv::Point2f pt);
} // namespace person_localize2d

#endif  