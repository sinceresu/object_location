#include "location_message_bridge_node.hpp"

namespace location_message_bridge
{
    CLocationMessageBridgeNode::CLocationMessageBridgeNode(NodeOptions _node_options)
        : node_options(_node_options)
    {
        mqtt_init(node_options.matt_address, node_options.matt_clientId,
                  node_options.matt_username, node_options.matt_password);

        // LaunchPublishers();
        LaunchSubscribers();
    }

    CLocationMessageBridgeNode::~CLocationMessageBridgeNode()
    {
    }

    void CLocationMessageBridgeNode::LaunchPublishers()
    {
        object_location_pose_Publisher = nh.advertise<object_localize3d_msgs::ObjectPose>(node_options.object_location_topic, 1);
    }

    void CLocationMessageBridgeNode::LaunchSubscribers()
    {
        object_location_pose_Subscriber = nh.subscribe(node_options.object_location_topic, 1,
                                                       &CLocationMessageBridgeNode::object_location_pose_Callback, this);
    }

    void CLocationMessageBridgeNode::object_location_pose_Callback(
        const object_localize3d_msgs::ObjectPose::ConstPtr &_msg)
    {
        // ROS_INFO_STREAM(*_msg);

        std::string message = "";
        if (msgRosToMqtt(*_msg, message) == 0)
        {
            object_location_pose_mqttPublish(message);
        }
    }

    // mqtt
    int CLocationMessageBridgeNode::mqtt_init(std::string _address, std::string _clientId,
                                              std::string _username, std::string _password)
    {
        // mqtt
        mqtt_base.init(_address, _clientId);
        mqtt_base.connect(_username, _password);

        // MqttPublishers();
        // MqttSubscribers();

        return 0;
    }

    void CLocationMessageBridgeNode::MqttPublishers()
    {
    }

    void CLocationMessageBridgeNode::MqttSubscribers()
    {
        mqtt_base.subscribe("/ydcloud/gw/+/iot/object/position/up", 0,
                            std::bind(&CLocationMessageBridgeNode::object_location_pose_mqttCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    }

    void CLocationMessageBridgeNode::object_location_pose_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length)
    {
        // printf("object_location_pose_mqttCallback - %s", _message.c_str());

        // SMqttObjectPose mqtt_object_pose;
        // x2struct::X::loadjson(_message, mqtt_object_pose, false);

        object_localize3d_msgs::ObjectPose object_pose;
        if (msgMqttToRos(_message, object_pose) == 0)
        {
            object_location_pose_Publisher.publish(object_pose);
        }
    }

    int CLocationMessageBridgeNode::object_location_pose_mqttPublish(std::string data)
    {
        Json::Reader reader;
        Json::Value data_json;
        if (!reader.parse(data, data_json))
        {
            std::cout << "json parse error" << std::endl;
            return -1;
        }
        time_t stamp = GetTimeStamp();
        std::string strStamp = std::to_string(stamp);
        std::string strLogId = strStamp;
        char md_c[32 + 1];
        if (md5_encoded(strStamp.c_str(), md_c) == 0)
        {
            strLogId = md_c;
        }
        Json::Value mqtt_json;
        mqtt_json["logId"] = strLogId;
        mqtt_json["gatewayId"] = node_options.device_id;
        mqtt_json["timestamp"] = (Json::Int64)stamp;
        mqtt_json["type"] = 0;
        mqtt_json["version"] = MQTT_BASE_VERSION;
        mqtt_json["data"] = data_json;
        std::string mqtt_str = mqtt_json.toStyledString();

        std::string topic_name = "/ydcloud/gw/" + node_options.device_id + "/iot/object/position/up";
        mqtt_base.publish(topic_name, 0, mqtt_str);

        return true;
    }

    //
    int CLocationMessageBridgeNode::msgRosToMqtt(const object_localize3d_msgs::ObjectPose &ros_msg, std::string &mqtt_msg)
    {
        int nRet = 0;
        if (ros_msg.object_poses.size() <= 0)
        {
            nRet = -1;
            return nRet;
        }

        SRosObjectPose object_pose;
        object_pose.header.seq = ros_msg.header.seq;
        object_pose.header.stamp.secs = ros_msg.header.stamp.toSec();
        object_pose.header.stamp.nsecs = ros_msg.header.stamp.toNSec();
        object_pose.header.frame_id = ros_msg.header.frame_id;
        object_pose.cam_id = ros_msg.cam_id;
        int object_count = ros_msg.object_poses.size();
        for (int object_index = 0; object_index < object_count; object_index++)
        {
            object_pose.object_ids.push_back(ros_msg.object_ids[object_index]);
            object_pose.object_types.push_back(ros_msg.object_types[object_index]);
            SRosPose pose;
            pose.position.x = ros_msg.object_poses[object_index].position.x;
            pose.position.y = ros_msg.object_poses[object_index].position.y;
            pose.position.z = ros_msg.object_poses[object_index].position.z;
            pose.orientation.x = ros_msg.object_poses[object_index].orientation.x;
            pose.orientation.y = ros_msg.object_poses[object_index].orientation.y;
            pose.orientation.z = ros_msg.object_poses[object_index].orientation.z;
            pose.orientation.w = ros_msg.object_poses[object_index].orientation.w;
            object_pose.object_poses.push_back(pose);
            // SRosPolygon area;
            // int point_count = ros_msg.areas[object_index].points.size();
            // for (int point_index = 0; point_index < point_count; point_index++)
            // {
            //     SRosPoint32 point;
            //     point.x = ros_msg.areas[object_index].points[point_index].x;
            //     point.y = ros_msg.areas[object_index].points[point_index].y;
            //     point.z = ros_msg.areas[object_index].points[point_index].z;
            //     area.points.push_back(point);
            // }
            // object_pose.areas.push_back(area);
        }

        std::string json_msg = x2struct::X::tojson(object_pose);
        // std::string json_msg = x2struct::X::tojson(object_pose, "", 4, ' ');
        mqtt_msg = json_msg;

        return nRet;
    }

    int CLocationMessageBridgeNode::msgMqttToRos(const std::string &mqtt_msg, object_localize3d_msgs::ObjectPose &ros_msg)
    {
        int nRet = 0;
        Json::Reader reader;
        Json::Value msg_json;
        if (reader.parse(mqtt_msg, msg_json))
        {
            Json::Value data_json = msg_json["data"];
            if (!data_json.isNull())
            {
                std::string data_str = data_json.toStyledString();
                SRosObjectPose temp_object_pose;
                x2struct::X::loadjson(data_str, temp_object_pose, false);

                object_localize3d_msgs::ObjectPose object_pose;
                object_pose.header.seq = temp_object_pose.header.seq;
                object_pose.header.stamp = ros::Time(temp_object_pose.header.stamp.secs, temp_object_pose.header.stamp.nsecs);
                object_pose.header.frame_id = temp_object_pose.header.frame_id;
                object_pose.cam_id = temp_object_pose.cam_id;
                int object_count = temp_object_pose.object_poses.size();
                for (int object_index = 0; object_index < object_count; object_index++)
                {
                    object_pose.object_ids.push_back(temp_object_pose.object_ids[object_index]);
                    object_pose.object_types.push_back(temp_object_pose.object_types[object_index]);
                    geometry_msgs::Pose pose;
                    pose.position.x = temp_object_pose.object_poses[object_index].position.x;
                    pose.position.y = temp_object_pose.object_poses[object_index].position.y;
                    pose.position.z = temp_object_pose.object_poses[object_index].position.z;
                    pose.orientation.x = temp_object_pose.object_poses[object_index].orientation.x;
                    pose.orientation.y = temp_object_pose.object_poses[object_index].orientation.y;
                    pose.orientation.z = temp_object_pose.object_poses[object_index].orientation.z;
                    pose.orientation.w = temp_object_pose.object_poses[object_index].orientation.w;
                    object_pose.object_poses.push_back(pose);
                    // geometry_msgs::Polygon area;
                    // int point_count = temp_object_pose.areas[object_index].points.size();
                    // for (int point_index = 0; point_index < point_count; point_index++)
                    // {
                    //     geometry_msgs::Point32 point;
                    //     point.x = temp_object_pose.areas[object_index].points[point_index].x;
                    //     point.y = temp_object_pose.areas[object_index].points[point_index].y;
                    //     point.z = temp_object_pose.areas[object_index].points[point_index].z;
                    //     area.points.push_back(point);
                    // }
                    // object_pose.areas.push_back(area);
                }
                ros_msg = object_pose;
            }
            else
            {
                nRet = -1;
                std::cout << "json data error" << std::endl;
            }
        }
        else
        {
            nRet = -1;
            std::cout << "json parse error" << std::endl;
        }

        return nRet;
    }
}
