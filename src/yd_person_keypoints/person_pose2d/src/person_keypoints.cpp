/**************************************************************************

Copyright: YiDa

Version: 1.00

Author: LiXingren

Date: 2022-05-18

Description: Input object detection results from object detection node.
             Output person keypoints result.
             2022-05-18: First edited.

**************************************************************************/
#include <iostream>
#include <fstream> // c++文件操作
#include <iomanip> // 设置输出格式
#include <exception>
#include "opencv2/opencv.hpp"
#include "time.h"
#include "ros/ros.h"
#include "object_detect2d_msgs/ObjectDetect2d.h"
#include "person_pose2d_msgs/PersonPose2d.h"
#include <cv_bridge/cv_bridge.h>
#include "personpose.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/daily_file_sink.h"

using namespace std;

#define __app_name__ "person_keypoints"

bool bTestMode = false;
double dRosRate = 100.0;
int iPersonTypeId;
ros::Publisher keypoints_pub;
static PersonPose *personPose;

void InitSpdlog()
{
    // auto daily_logger = spdlog::daily_logger_mt("detection_pose_cpu", "log/daily.txt", 0, 0);
    spdlog::set_level(spdlog::level::info);
    // spdlog::set_default_logger(daily_logger);
}

void Sleep(int ms)
{
    struct timeval delay;
    delay.tv_sec = 0;
    delay.tv_usec = ms * 1000;
    select(0, NULL, NULL, NULL, &delay);
}

std::vector<int16_t> Detect_Keypoints(cv::Mat image, std_msgs::Header header)
{
    try
    {
        std::vector<int16_t> person_keypoints;
        int iOriW = image.cols;
        int iOriH = image.rows;
        cv::Mat out;
        cv::Scalar value = cv::Scalar(255, 255, 255);
        if (iOriH * personPose->W >= personPose->H * iOriW)
        {
            cv::copyMakeBorder(image, out, 0, 0, 0, iOriH * personPose->W / personPose->H - iOriW, cv::BORDER_CONSTANT, value);
        }
        else
        {
            cv::copyMakeBorder(image, out, 0, iOriW * personPose->H / personPose->W - iOriH, 0, 0, cv::BORDER_CONSTANT, value);
        }

        // double dResizeW = (double)iOriW / personPose->W;
        // double dResizeH = (double)iOriH / personPose->H;
        // spdlog::info("out size,height:{},wight:{}", out.rows, out.cols);

        std::vector<float> out_result = personPose->DoInference(out);
        if (out_result.size() <= 0)
        {
            return person_keypoints;
        }

        if (out_result.size() / 75 != 1)
        {
            // spdlog::warn("single person error pose count:{}", out_result.size() / 75);
        }

        bool bSuc = false;

        float fMaxConfidenceSum = 0;
        int iMaxConfidenceIndex = 0;

        for (int i = 0; i < out_result.size() / 75; i++)
        {
            float fValue = 0;
            for (int k = 0; k < 75; k++)
            {
                if ((k + 1) % 3 == 0)
                {
                    fValue += out_result[k + i * 75];
                }
            }
            if (fValue >= fMaxConfidenceSum)
            {
                fMaxConfidenceSum = fValue;
                iMaxConfidenceIndex = i;
            }
        }
        for (int i = iMaxConfidenceIndex * 75; i < (iMaxConfidenceIndex + 1) * 75; i++)
        {
            if (i % 3 == 2)
            {
                person_keypoints.push_back(int16_t(out_result[i] * 100));
            }
            else
            {
                person_keypoints.push_back(int16_t(out_result[i]));
            }
        }

        if (bTestMode)
        {
            // time_t t = time(0);
            // char tmp[32] = {NULL};
            // strftime(tmp, sizeof(tmp), "%Y-%m-%d %H:%M:%S", localtime(&t));
            // string strTemp = tmp;
            string strOutputImagePath = "/home/lxr/workdir/src/yd_person_angle/test_data/" +
                                        to_string(header.stamp.sec) + "_" + to_string(header.stamp.nsec) + "_cut.jpg";
            // string strOutputImagePath = "/home/lxr/workdir/src/yd_person_keypoints/person_pose2d/database/" +
            //                             to_string(t) + ".jpg";
            string strTestTxtPath = "/home/lxr/workdir/src/yd_person_angle/test_data/" + to_string(header.stamp.sec) + "_" + to_string(header.stamp.nsec) + ".txt";
            ofstream out_txt_file;
            out_txt_file.open(strTestTxtPath, ios::out | ios::trunc);
            out_txt_file << fixed;
            for (int i = 0; i < 25; i++)
            {
                std::cout << "x:" << person_keypoints[i * 3] << ",y:" << person_keypoints[i * 3 + 1] << ",prop:" << person_keypoints[i * 3 + 2] << std::endl;
                cv::circle(out, cv::Point(person_keypoints[i * 3], person_keypoints[i * 3 + 1]), 3, cv::Scalar(225, 0, 225), 3, 8);
                cv::putText(out, std::to_string(i), cv::Point(person_keypoints[i * 3] + 10, person_keypoints[i * 3 + 1] + 10), cv::FONT_HERSHEY_COMPLEX, 0.3, 2);

                out_txt_file << person_keypoints[i * 3] << endl;
                out_txt_file << person_keypoints[i * 3 + 1] << endl;
            }
            out_txt_file.close();

            cv::imwrite(strOutputImagePath, out);
        }
        
        out.release();
        return person_keypoints;
    }
    catch (std::exception &e)
    {
        spdlog::error("Standard exception:{}", e.what());
    }
}

void detection_result_callback(const object_detect2d_msgs::ObjectDetect2d &_object_detect2d_msg)
{
    try
    {
        struct timeval tstart, tend;
        double timeUsed;
        gettimeofday(&tstart, NULL);

        person_pose2d_msgs::PersonPose2d out_person_poses;
        int iObjectCnt = _object_detect2d_msg.object_types.size();
        for (int i = 0; i < iObjectCnt; i++)
        {
            out_person_poses.object_ids.push_back(_object_detect2d_msg.object_ids[i]);
            out_person_poses.object_types.push_back(_object_detect2d_msg.object_types[i]);
            int iStartX = _object_detect2d_msg.object_boxes[i].points[0].x;
            int iStartY = _object_detect2d_msg.object_boxes[i].points[0].y;
            int iEndX = _object_detect2d_msg.object_boxes[i].points[1].x;
            int iEndY = _object_detect2d_msg.object_boxes[i].points[1].y;

            if (iPersonTypeId != _object_detect2d_msg.object_types[i])
            {
                out_person_poses.person_poses.push_back(_object_detect2d_msg.object_boxes[i]);
            }
            else
            {
                cv_bridge::CvImagePtr cv_ptr;
                try
                {
                    cv_ptr = cv_bridge::toCvCopy(_object_detect2d_msg.object_images[i], sensor_msgs::image_encodings::BGR8);
                }
                catch (cv_bridge::Exception &e)
                {
                    ROS_ERROR("cv_bridge exception: %s", e.what());
                    continue;
                }
                catch (std::exception &e)
                {
                    std::cout << "Standard exception: " << e.what() << std::endl;
                }

                std::vector<int16_t> person_keypoints = Detect_Keypoints(cv_ptr->image, _object_detect2d_msg.header);
                if (person_keypoints.size() <= 0)
                {
                    continue;
                }

                geometry_msgs::Polygon polygon;
                for (int i = 0; i < 25; i++)
                {
                    geometry_msgs::Point32 point;
                    if (person_keypoints[i * 3 + 2] <= 0)
                    {
                        point.x = 0;
                        point.y = 0;
                    }
                    else
                    {
                        point.x = iStartX + person_keypoints[i * 3];
                        point.y = iStartY + person_keypoints[i * 3 + 1];
                    }

                    polygon.points.push_back(point);
                }
                // add rect
                geometry_msgs::Point32 pointLeft, pointRight;
                pointLeft.x = iStartX;
                pointLeft.y = iStartY;
                pointRight.x = iEndX;
                pointRight.y = iEndY;
                polygon.points.push_back(pointLeft);
                polygon.points.push_back(pointRight);
                // spdlog::info("person keypoints count:{}", polygon.points.size());
                out_person_poses.person_poses.push_back(polygon);
                cv_ptr->image.release();
            }
        }

        for (int i = 0; i < out_person_poses.person_poses.size(); i++)
        {
            if (out_person_poses.person_poses[i].points.size() < 27)
            {
                spdlog::info("person keypoints count:{},object type:{}", out_person_poses.person_poses[i].points.size(), out_person_poses.object_types[i]);
            }
        }

        out_person_poses.header = _object_detect2d_msg.header;
        out_person_poses.cam_id = _object_detect2d_msg.cam_id;
        keypoints_pub.publish(out_person_poses);

        gettimeofday(&tend, NULL);
        timeUsed = (tend.tv_sec - tstart.tv_sec) + (tend.tv_usec - tstart.tv_usec) * 0.000001;
        // spdlog::info("compute person keypoints cost time(us): {}", timeUsed);
    }
    catch (std::exception &e)
    {
        spdlog::error("Standard exception:{}", e.what());
    }
}

int main(int argc, char **argv)
{
    try
    {
        InitSpdlog();

        ros::init(argc, argv, __app_name__);
        ros::Time::init();
        ros::NodeHandle node_handle;
        std::string strDetectResultTopic, strKeypointsResultTopic, strHeartbeatTopic, strModelDir, strOutputPersonId;
        ros::param::get("test_mode", bTestMode);

        ros::param::get("detect_result_topic", strDetectResultTopic);
        ros::param::get("keypoints_result_topic", strKeypointsResultTopic);
        ros::param::get("heartbeat_topic", strHeartbeatTopic);
        ros::param::get("model_dir", strModelDir);
        ros::param::get("output_person_id", iPersonTypeId);
        ros::param::get("ros_rate", dRosRate);

        ros::Subscriber subDetectResult = node_handle.subscribe(strDetectResultTopic, 1, &detection_result_callback);
        keypoints_pub = node_handle.advertise<person_pose2d_msgs::PersonPose2d>(strKeypointsResultTopic, 1);

        personPose = new PersonPose();
        personPose->InitEngine(strModelDir);

        ros::Rate rate(dRosRate);

        while (ros::ok())
        {
            ros::spinOnce();
            rate.sleep();
        }
    }
    catch (std::exception &e)
    {
        spdlog::error("Standard exception:{}", e.what());
    }

    spdlog::shutdown();

    return 0;
}
