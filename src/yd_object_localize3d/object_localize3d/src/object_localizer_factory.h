#pragma once
#include <string>
#include <deque>
#include <set>


#include <pcl/filters/voxel_grid_occlusion_estimation.h>

namespace object_location{

namespace object_localize3d{
class ObjectLocalizer;
class ObjectLocalizerFactory 
{
public:
  static std::shared_ptr<ObjectLocalizer> CreateObjectLocalizer(std::string object_label) ;
 };
} // namespaceobject_location
}// namespaceobject_location
