#include "rotate_cam_param_reader.h"

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/core/eigen.hpp>

#include "glog/logging.h"

using namespace std;

namespace object_location
{
    namespace object_localize3d
    {

        RotateCamParamReader::RotateCamParamReader()
        {
        }

        RotateCamParamReader::~RotateCamParamReader()
        {
        }

        int RotateCamParamReader::LoadExtrinsic(cv::FileStorage _fs)
        {
            axis_mats_.clear();
            int num = _fs["num"];
            for (int i = 0; i < num; ++i)
            {
                cv::Mat cv_RT;
                string index("Matrix_");
                index = index + to_string(i);
                _fs[index] >> cv_RT;
                Eigen::Matrix4d eigen_RT;
                cv::cv2eigen(cv_RT, eigen_RT);
                axis_mats_.push_back(eigen_RT);
            }
            cv::Mat cv_RT;
            _fs["Tool"] >> cv_RT;
            cv::cv2eigen(cv_RT, tool_mat_);

            // return 0;

            std::vector<double> rads(axis_mats_.size(), 0.0);
            SetRotations(rads);

            return 0;
        }

        int RotateCamParamReader::SetRotations(const vector<double> &_rads)
        {
            assert(axis_mats_.size() == _rads.size());
            int num = axis_mats_.size();
            extrinsic_mat_.setIdentity();
            Eigen::Matrix4d motionframes;
            for (int i = 0; i < num; i++)
            {
                motionframes = Eigen::Matrix4d::Identity();
                double rad = _rads[i] / 100;
                rad = rad > 180 ? rad - 360 : rad;
                motionframes.block<3, 3>(0, 0) = (Eigen::AngleAxisd(rad * M_PI / 180.0, Eigen::Vector3d::UnitZ())).matrix();
                extrinsic_mat_ = extrinsic_mat_ * axis_mats_[i] * motionframes;
            }
            extrinsic_mat_ = extrinsic_mat_ * tool_mat_;

            return 0;
        }

    }

}
