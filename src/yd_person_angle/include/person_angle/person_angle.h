/**************************************************************************

Copyright: YiDa

Version: 1.00

Author: LiXingren

Date: 2022-05-18

Description: Input person keypoints.
             Output angle along the photo and angle vertical with the surface of photo.
             2022-05-18: First edited.

**************************************************************************/
#include <iostream>
#include <exception>
#include <vector>
#include <stdint.h>
#include <math.h>
#include <opencv2/opencv.hpp>


#define PI 3.14159265

namespace PersonAngleLib
{
    class PersonAngle
    {
    private:
            /* data */
        int iX = 1280;
        int iY = 780;
    public:
        PersonAngle(/* args */);
        ~PersonAngle();
         int LoadCalibrationFile(const std::string& calibration_filepath);

        bool ComputeOrientation(const std::vector<cv::Point2i>& vKeypoints, cv::Vec3f & person_orientation);
        bool Compute2DOrientation(const std::vector<cv::Point2i>& vKeypoints, cv::Vec2f & person_orientation);

    private:
         cv::Mat camera_intrinsic_;
         cv::Mat camera_distortion_;

    };
} // namespace PersonAngleLib
