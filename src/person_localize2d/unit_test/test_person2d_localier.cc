#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>

#include <opencv2/opencv.hpp>


#include "gflags/gflags.h"
#include "glog/logging.h"

#include "person_localize2d/person_localize2d_interface.h"
#include "person_localize2d/libperson_localize2d.h"

DEFINE_string(
    joint_filepath, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");              

DEFINE_string(
    image_filepath, "",
"Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");


using namespace std;
using namespace cv;

namespace person_localize2d {

namespace {
vector<Point2i> readJoints(const char *joint_filepath)
{ 
  vector<Point2i> result;

  // Read numbers from file into buffer.
  ifstream infile;
  infile.open(joint_filepath);
  while (! infile.eof())  {
    string line;
    getline(infile, line);

    int temp_cols = 0;
    Point2f point;
    stringstream stream(line);
    stream >>point.x >> point.y;

    result.emplace_back(point.x, point.y);

  }

  infile.close();
  
  return result;
}

}


void Test(int argc, char** argv) {
    auto person_localize2d = CreatePerson2dLocalizer();

    cv::Mat image = cv::imread(FLAGS_image_filepath);
    if (image.empty()){
      
    }

    auto person_joints  = readJoints(FLAGS_joint_filepath.c_str());

    for (int i = 0;  i < person_joints.size(); i++) {
        circle(image, person_joints[i], 5, Scalar(255u, 0u, 0u));
    }
   

    Point2i person_position;
    cv::Rect2i person_box(0, 0, 0, 0);

    person_localize2d->Localize(person_joints, person_box, person_position);
    circle(image, person_position, 5, Scalar(0u,255u,  0u));
    imwrite( "output.png", image);
}

    auto person_localize2d = CreatePerson2dLocalizer();
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;


  person_localize2d::Test(argc, argv);

}
