#include <stdio.h>
#include <math.h>
#include <csignal>
#include <unistd.h>
#include <string>

#include "location_message_bridge_node.hpp"

void sig_handler(int sig)
{
    if (sig == SIGINT)
    {
        exit(0);
    }
}

namespace location_message_bridge
{
    void run()
    {
        CLocationMessageBridgeNode::NodeOptions node_options;
        ros::param::get("~device_id", node_options.device_id);

        ros::param::get("~object_location_topic", node_options.object_location_topic);

        ros::param::get("~matt_address", node_options.matt_address);
        ros::param::get("~matt_clientId", node_options.matt_clientId);
        ros::param::get("~matt_username", node_options.matt_username);
        ros::param::get("~matt_password", node_options.matt_password);

        CLocationMessageBridgeNode location_message_bridge_node(node_options);

        ROS_INFO("message bridge service node started.");

        ros::spin();
        // ros::Rate loop_rate(1);
        // while (ros::ok())
        // {
        //     ros::spinOnce();
        //     loop_rate.sleep();
        // }
    }
}

int main(int argc, char **argv)
{
    signal(SIGINT, sig_handler);

    setlocale(LC_CTYPE, "zh_CN.utf8");
    setlocale(LC_ALL, "");

    std::string run_path = "";
    char *buffer;
    if ((buffer = getcwd(NULL, 0)) == NULL)
    {
        run_path = ".";
    }
    else
    {
        run_path = buffer;
        free(buffer);
    }

    std::string pack_path = "";
    pack_path = ros::package::getPath("location_message_bridge");

    ros::init(argc, argv, "location_message_bridge");
    ros::Time::init();

    location_message_bridge::run();

    ros::shutdown();

    return 0;
}