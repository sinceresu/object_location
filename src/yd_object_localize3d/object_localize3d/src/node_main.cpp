/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ros/ros.h"

#include "glog/logging.h"
#include "gflags/gflags.h"
#include "ros_log_sink.h"


#include "location_node.h"



DEFINE_string(calibration_filepath, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(cloud_map_filepath, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

namespace object_location {
namespace  object_localize3d{
void Run() {

  bool ptz_rotate;
  std::string ptz_pose_topic;
  std::string detect_topic;
  std::string poseKeypoints_topic;
  std::string location_topic;
  std::string frame_id;

  float horizontal_fov;
  float vertical_fov;
  float near_plane_distance;
  float far_plane_distance;
  std::vector<int> object_types;
  std::vector<std::string> object_labels;
  std::vector<float> object_heights;

  //extract floor paramter
  bool extract_floor;
  bool extract_only_floor;
  bool use_normal_filtering;
  bool use_upsample_filtering;
  float sensor_height;
  float height_clip_range;

  ros::param::get("~ptz_rotate", ptz_rotate);

  ros::param::get("~ptz_pose_topic", ptz_pose_topic);
  ros::param::get("~detect_topic", detect_topic);
  ros::param::get("~poseKeypoints_topic", poseKeypoints_topic);
  ros::param::get("~location_topic", location_topic);
  ros::param::get("~frame_id", frame_id);

  ros::param::get("~horizontal_fov", horizontal_fov);
  ros::param::get("~vertical_fov", vertical_fov);
  ros::param::get("~near_plane_distance", near_plane_distance);
  ros::param::get("~far_plane_distance", far_plane_distance);
  ros::param::get("~object_types", object_types);
  ros::param::get("~object_labels", object_labels);
  ros::param::get("~object_heights", object_heights);

  ros::param::get("~extract_floor", extract_floor);
  ros::param::get("~extract_only_floor", extract_only_floor);
  ros::param::get("~use_normal_filtering", use_normal_filtering);
  ros::param::get("~use_upsample_filtering", use_upsample_filtering);

   ros::param::get("~sensor_height", sensor_height);
  ros::param::get("~height_clip_range", height_clip_range);

  LOG(INFO) << "Pose position node started.";

  LocationNode::LocationNodeOptions node_options;
  node_options.calibration_filepath = FLAGS_calibration_filepath;
  node_options.cloud_map_filepath = FLAGS_cloud_map_filepath;
  node_options.ptz_rotate = ptz_rotate;
  node_options.ptz_pose_topic = ptz_pose_topic;
  node_options.detect_topic = detect_topic;
  node_options.poseKeypoints_topic = poseKeypoints_topic;
  node_options.location_topic = location_topic;
  node_options.frame_id = frame_id;

  node_options.horizontal_fov = horizontal_fov;
  node_options.vertical_fov = vertical_fov;
  node_options.near_plane_distance = near_plane_distance;
  node_options.far_plane_distance = far_plane_distance;
  node_options.object_types = object_types;
  node_options.object_labels = object_labels;
  node_options.object_heights = object_heights;

  node_options.extract_floor = extract_floor;
  node_options.extract_only_floor = extract_only_floor;
  node_options.use_normal_filtering = use_normal_filtering;
  node_options.use_upsample_filtering = use_upsample_filtering;
  node_options.sensor_height = sensor_height;
  node_options.height_clip_range = height_clip_range;


  LocationNode location_node(node_options);

  ::ros::spin();

}

}
}

int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  // CHECK(!FLAGS_configuration_directory.empty())
  //     << "-configuration_directory is missing.";
  // CHECK(!FLAGS_configuration_basename.empty())
  //     << "-configuration_basename is missing.";

  ::ros::init(argc, argv, "cartographer_node");
  ::ros::start();
  object_location::object_localize3d::ScopedRosLogSink ros_log_sink;
  
  object_location::object_localize3d::Run();

  ::ros::shutdown();
}
