/*
version：0.10
author：lixingren
用于上海项目多摄像头人员检测

2021-07-26：修改了部分代码，打印出中间信息以追踪漏检等情况。
2021-08-18：修改了时间打印和未检测到时，发送检测框的三分坐标点。

实际使用需修改130行左右的三行配置路径。
 */
#include "personpose.h"
#include <vector>

#include <unistd.h>
#include <dirent.h>

#include "OpenPose.hpp"

class PersonPose::Impl
{
public:
    Impl() {}

    ~Impl() {}

    OpenPose *openpose;
};

PersonPose::PersonPose()
{
    _impl = new Impl();
}

PersonPose::~PersonPose()
{
    if (_impl)
    {
        delete _impl;
        _impl = nullptr;
    }
}

std::vector<float> PersonPose::DoInference(cv::Mat img)
{
    int N = 1;
    int C = 3;

    // cv::cvtColor(img, img, cv::COLOR_BGR2RGB);
    int iOriW = img.cols;
    int iOriH = img.rows;
    cv::resize(img, img, cv::Size(W, H));

    std::vector<float> inputData;
    inputData.resize(N * C * H * W);

    // cv::Mat img
    unsigned char *data = img.data;

    // int iImageSize = W * H;
    int iImageSize = img.rows * img.cols;
    for (int n = 0; n < N; n++)
    {
        for (int c = 0; c < 3; c++)
        {
            for (int i = 0; i < iImageSize; i++)
            {
                inputData[i + c * W * H + n * 3 * H * W] = (float)data[i * 3 + c];
            }
        }
    }
    std::vector<float> results;
    _impl->openpose->DoInference(inputData, results);

    img.release();
    std::vector<float> out_results;
    int iResultCnt = results.size();
    for (int i = 0; i < iResultCnt; i++)
    {
        int index = i % 3;
        if (index == 0)
        {
            out_results.push_back(results[i] * iOriW / W);
        }
        else if (index == 1)
        {
            out_results.push_back(results[i] * iOriH / H);
        }
        else
        {
            out_results.push_back(results[i]);
        }
    }

    return out_results;
}

int PersonPose::InitEngine(string &strModelDir)
{
    // char buff[FILENAME_MAX];
    // if(!getcwd(buff, FILENAME_MAX))
    // {
    //     perror("getcwd");
    //     return 1;
    // }
    // std::string current_working_dir(buff);
    // std::string strDeploy = strModelDir + "/pose_deploy.prototxt";
    // std::string strCaffeModel = strModelDir + "/pose_iter_584000.caffemodel";
    // std::string strEngin = strModelDir + "/trt.engine";

    const std::string &prototxt = strModelDir + "/pose_deploy.prototxt";
    const std::string &caffemodel = strModelDir + "/pose_iter_584000.caffemodel";
    const std::string &save_engine = strModelDir + "/trt.engine";

    std::cout << "###################prototxt" << prototxt << std::endl;
    std::cout << "###################strModelDir" << strModelDir << std::endl;

    int N = 1;

    std::vector<std::string> outputBlobname{"net_output"};
    std::vector<std::vector<float>> calibratorData;
    calibratorData.resize(3);
    for (size_t i = 0; i < calibratorData.size(); i++)
    {
        calibratorData[i].resize(3 * H * W);
        for (size_t j = 0; j < calibratorData[i].size(); j++)
        {
            calibratorData[i][j] = 0.05f;
        }
    }
    int maxBatchSize = N;
    std::cout << "###################start openpose";
    _impl->openpose = new OpenPose(prototxt,
                                   caffemodel,
                                   save_engine,
                                   outputBlobname,
                                   calibratorData,
                                   maxBatchSize);
    return 0;
}
