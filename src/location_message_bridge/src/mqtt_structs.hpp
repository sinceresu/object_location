#ifndef mqtt_structs_HPP
#define mqtt_structs_HPP

#pragma once
#include <string>
#include <vector>
#include "jsoncpp/json/json.h"
#include "x2struct/x2struct.hpp"

#include "ros_structs.hpp"

typedef struct MqttObjectPose
{
    std::string logId;
    std::string gatewayId;
    double timestamp;
    int type;
    std::string version;
    SRosObjectPose data;
    XTOSTRUCT(O(logId, gatewayId, timestamp, type, version, data));
} SMqttObjectPose, *PMqttObjectPose;

#endif
