#ifndef location_message_bridge_node_HPP
#define location_message_bridge_node_HPP

#pragma once
#include <queue>
#include <mutex>
#include "ros/ros.h"
#include "ros/package.h"

#include "Common.hpp"
#include "mqtt_base.hpp"
#include "ros_structs.hpp"
#include "mqtt_structs.hpp"

#include "object_localize3d_msgs/ObjectPose.h"

namespace location_message_bridge
{
    class CLocationMessageBridgeNode
    {
    public:
        typedef struct _NodeOptions
        {
            std::string device_id;
            //
            std::string object_location_topic;
            //
            std::string matt_address;
            std::string matt_clientId;
            std::string matt_username;
            std::string matt_password;
        } NodeOptions;

    public:
        CLocationMessageBridgeNode(NodeOptions _node_options);
        ~CLocationMessageBridgeNode();

    private:
        NodeOptions node_options;

    private:
        ros::NodeHandle nh;
        ros::NodeHandle private_nh;

        ros::Publisher object_location_pose_Publisher;

        // Subscriber
        ros::Subscriber object_location_pose_Subscriber;
        void object_location_pose_Callback(const object_localize3d_msgs::ObjectPose::ConstPtr &_msg);

        void LaunchPublishers();
        void LaunchSubscribers();

    private:
        CMqttBase mqtt_base;
        int mqtt_init(std::string _address, std::string _clientId,
                      std::string _username, std::string _password);
        void object_location_pose_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length);

        int object_location_pose_mqttPublish(std::string data);

        void MqttPublishers();
        void MqttSubscribers();

    private:
        int msgRosToMqtt(const object_localize3d_msgs::ObjectPose &ros_msg, std::string &mqtt_msg);
        int msgMqttToRos(const std::string &mqtt_msg, object_localize3d_msgs::ObjectPose &ros_msg);
    };
}

#endif