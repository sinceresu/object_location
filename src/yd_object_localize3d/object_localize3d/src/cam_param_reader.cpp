#include "cam_param_reader.h"

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/core/eigen.hpp>

#include "glog/logging.h"

using namespace std;

namespace object_location
{
  namespace object_localize3d
  {

    CamParamReader::CamParamReader()
    {
    }

    CamParamReader::~CamParamReader()
    {
    }

    int CamParamReader::LoadIntrinsic(cv::FileStorage _fs)
    {
      if (!_fs.isOpened())
      {
        LOG(FATAL) << "can't open calibration file.";
        return -1;
      }

      _fs["CameraMat"] >> intrinsic_mat_;
      _fs["DistCoeff"] >> distortion_coeffs_;
      _fs["ImageSize"] >> image_size_;

      return 0;
    }

    int CamParamReader::LoadExtrinsic(cv::FileStorage _fs)
    {
      if (!_fs.isOpened())
      {
        LOG(FATAL) << "can't open calibration file.";
        return -1;
      }

      cv::Mat cam_extrinsic_mat;
      _fs["CameraExtrinsicMat"] >> cam_extrinsic_mat;
      cv::cv2eigen(cam_extrinsic_mat, extrinsic_mat_);

      return 0;
    }

    int CamParamReader::LoadCalibrationFile(const std::string &_calibration_file)
    {
      cv::FileStorage fs;
      fs.open(_calibration_file, cv::FileStorage::READ);
      if (!fs.isOpened())
      {
        LOG(FATAL) << "can't open calibration file " << _calibration_file << ".";
        return -1;
      }

      int result = LoadIntrinsic(fs);
      if (result != 0)
        return -1;

      result = LoadExtrinsic(fs);

      return result;
    }

    int CamParamReader::SetRotations(const vector<double> &_rads)
    {
      return 0;
    }

  }

}