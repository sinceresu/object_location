/**************************************************************************

Copyright: YiDa

Version: 1.00

Author: LiXingren

Date: 2022-05-18

Description: Description: Input rtsp stream. Output ros image.
             2022-05-18: First edited.

**************************************************************************/

#include <exception>
#include "opencv2/opencv.hpp"
#include <cv_bridge/cv_bridge.h>
#include "visible_stream.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/daily_file_sink.h"

namespace VisibleStreamNode
{
    VisibleStream::VisibleStream()
    {
        auto daily_logger = spdlog::daily_logger_mt(__app_name__, "log/daily.txt", 0, 0);
        spdlog::set_level(spdlog::level::info);
        // spdlog::flush_on(spdlog::level::info);
        // spdlog::set_default_logger(daily_logger);
        ros::param::get("test_mode", bTestMode);
        if (bTestMode)
        {
            cv::namedWindow(__app_name__, cv::WINDOW_NORMAL);
            cv::resizeWindow(__app_name__, 640, 480);
        }
    }

    VisibleStream::~VisibleStream()
    {
        if (bTestMode)
        {
            cv::destroyAllWindows();
        }
        cap.release();
        spdlog::info("shut down");
        spdlog::shutdown();
    }

    void VisibleStream::Init()
    {
        try
        {
            ros::NodeHandle node_handle;
            std::string strUrl, strVisibleStreamTopic, strHeartbeat, strUserName, strPassword, strRtspIp, strRtspPort, strImageInterval, strImageWidth, strImageHeight;
            double dModelThresh;
            ros::param::get("rtsp", strUrl);
            ros::param::get("ros_rate", dRosRate);

            ros::param::get("visible_stream_topic", strVisibleStreamTopic);
            ros::param::get("heartbeat_topic", strHeartbeat);
            ros::param::get("user_name", strUserName);
            ros::param::get("password", strPassword);
            ros::param::get("rtsp_ip", strRtspIp);
            ros::param::get("rtsp_port", strRtspPort);
            ros::param::get("image_interval", strImageInterval);
            ros::param::get("image_width", strImageWidth);
            ros::param::get("image_height", strImageHeight);

            if (bTestMode)
            {
            }
            else
            {
                // strUrl = "rtspsrc location=rtsp://";
                // strUrl.append(strUserName);
                // strUrl.append(":");
                // strUrl.append(strPassword);
                // strUrl.append("@");
                // strUrl.append(strRtspIp);
                // strUrl.append(":");
                // strUrl.append(strRtspPort);
                // strUrl.append("/h264/ch1/main/av_stream latency=20 ! queue ! rtph264depay ! h264parse ! nvv4l2decoder");
                // if (std::stoi(strImageInterval) > 1)
                // {
                //     strUrl.append(" drop-frame-interval=");
                //     strUrl.append(strImageInterval);
                // }
                // strUrl.append(" ! nvvideoconvert ! video/x-raw,format=(string)I420 ! queue ! appsink");

                // strUrl = "rtsp://";
                // strUrl.append(strUserName);
                // strUrl.append(":");
                // strUrl.append(strPassword);
                // strUrl.append("@");
                // strUrl.append(strRtspIp);
                // strUrl.append(":");
                // strUrl.append(strRtspPort);
                // strUrl.append("/h264/ch1/main/av_stream");
            }

            pubVisibleStream = node_handle.advertise<sensor_msgs::Image>(strVisibleStreamTopic, 1);

            cap.open(strUrl);
        }
        catch (std::exception &e)
        {
            spdlog::error("Standard exception:{}", e.what());
        }
    }

    void VisibleStream::Update()
    {
        try
        {
            if (cap.isOpened())
            {
                struct timeval t1, t2;
                double timeuse;
                gettimeofday(&t1, NULL);
                bool bResult = cap.grab();
                if (!bResult)
                {
                    return;
                }

                cv::Mat cvImage;
                cap.retrieve(cvImage);
                cv_bridge::CvImage cvi;
                ros::Time time = ros::Time::now();
                cvi.header.stamp = time;
                cvi.header.frame_id = "image";
                cvi.encoding = "bgr8";
                cvi.image = cvImage;
                sensor_msgs::Image im;
                cvi.toImageMsg(im);
                pubVisibleStream.publish(im);

                if (bTestMode)
                {
                    cv::imshow(__app_name__, cvImage);
                    cv::waitKey(1);
                    cvImage.release();
                }
                gettimeofday(&t2, NULL);
                timeuse = (t2.tv_sec - t1.tv_sec) * 1000 * 1000 + (double)(t2.tv_usec - t1.tv_usec);
                spdlog::info("handle image cost time(us):{}", timeuse); //输出时间（单位：uｓ）
            }
        }
        catch (std::exception &e)
        {
            spdlog::error("Standard exception:{}", e.what());
        }
    }
}

int main(int argc, char **argv)
{
    try
    {
        ros::init(argc, argv, __app_name__);
        ros::Time::init();

        VisibleStreamNode::VisibleStream visibleStream;
        visibleStream.Init();
        ros::Rate rate(visibleStream.dRosRate);

        while (ros::ok())
        {
            visibleStream.Update();
            ros::spinOnce();
            rate.sleep();
        }
    }
    catch (std::exception &e)
    {
        std::cout << "Standard exception:" << e.what() << std::endl;
    }
    return 0;
}
