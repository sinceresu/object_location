#include "person_localizer.h"


using namespace std;

namespace object_location{

namespace object_localize3d{
namespace {
const float HALF_BODY_RATIO_DOWN_THRESHOLD= 0.5f;
const float HALF_BODY_RATIO_UP_THRESHOLD= 1.2f;
const float PERSON_HEAD_HEIGHT = 1.7f;
const float EDEG_RATIO=  0.02f;

}


PersonLocalizer::PersonLocalizer() 

{
    // std::vector<uint32_t >image_ids = {1,2,3,4,5,6,7,8,9,10};
    // image_id_set = std::set<uint32_t>(image_ids.begin(), image_ids.end());
}

PersonLocalizer::~PersonLocalizer() {
    // std::cout << "image_ids" <<  std::endl; 
    // std::ofstream image_id_f("image_id.txt");
    // for (const auto image_id : image_id_set) {
    //     image_id_f << image_id << "," ; 
    // }

}

void PersonLocalizer::GetObjectPosition(const cv::Rect2f&  object_region, cv::Point2f&  object_position2d, double & object_height) {
  if (!IsWholeBody(object_region) && IsAtBottom(object_region)) {
    object_position2d = cv::Point2f{object_region.x + object_region.width / 2, object_region.y}; 
    object_height_ = PERSON_HEAD_HEIGHT;
    return;
  }
  return ObjectLocalizer::GetObjectPosition(object_region, object_position2d);
}

bool PersonLocalizer::IsWholeBody(const cv::Rect2f& object_region) {
  float ratio = object_region.width / object_region.width;
  return ratio < HALF_BODY_RATIO_DOWN_THRESHOLD;
}

bool PersonLocalizer::IsAtBottom(const cv::Rect2f& object_region) {
  auto position_2d = cv::Point2f{object_region.x + object_region.width / 2, object_region.y + object_region.height}; 
  return position_2d.y > ( 1.0f - EDEG_RATIO) *image_size_.height ;
}

} // namespace object_location
}// namespace object_location
