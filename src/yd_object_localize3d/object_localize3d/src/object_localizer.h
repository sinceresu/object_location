#pragma once
#include <string>
#include <deque>
#include <set>
#include <queue>
#include <mutex>
#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/frustum_culling.h>
#include <pcl/filters/voxel_grid_occlusion_estimation.h>

namespace object_location{

namespace object_localize3d{

class CamParamReader;
class RotateCamParamReader;

class ObjectLocalizer 
{
public:
  explicit ObjectLocalizer() ;

  virtual ~ObjectLocalizer();

  ObjectLocalizer(const ObjectLocalizer&) = delete;
  ObjectLocalizer& operator=(const ObjectLocalizer&) = delete;

  int SetCloudMap(pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud_map);
  int LoadCalibrationFile(const std::string& calibration_filepath, bool rotate = false);
  int SetCullingParamters( float horizontal_fov, 
                          float vertical_fov, 
                          float near_plane_distance, 
                          float far_plane_distance) ;
void SetParamters( double object_height) ;
int Localize( const cv::Rect2f&  object_region, cv::Point3f& object_position);
int LocalizePoint( const cv::Point2f& object_position_2d, cv::Point3f& object_position);
int LocalizeOrientation( const cv::Vec3f&  object_orientation_2d, cv::Vec3f&object_orientation);

cv::Point2f  Project3dPointToImage( const cv::Point3f& object_position);

public:
  int SetRotations(const std::vector<double> rotations);
  void UpdateExtrinsic();

protected:
  std::shared_ptr<CamParamReader> cam_param_reader_;
  std::mutex extrinsic_mutex_;
  bool extrinsic_need_update_;

protected:
virtual void GetObjectPosition(const cv::Rect2f&  object_region, cv::Point2f&  object_position3d);

 pcl::PointCloud<pcl::PointXYZ>::Ptr CullingPointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr point_cloud, const Eigen::Matrix4f& parent_link_pose) ;

  int GenerateDistanceImage();
  cv::Point3f AdjustLocation(const cv::Point3f&object_position);

  // void InitializeOcclusionFilter(pcl::PointCloud<>::ConstPtr occlude_pcl, const Eigen::Matrix4f& ref_to_world, int64_t image_id);
  cv::Mat intrinsic_mat_;  
  cv::Mat distortion_coeffs_;
  cv::Mat extrinsic_mat_;  
  cv::Size image_size_;
  Eigen::Matrix4f camara_to_map_pose_;
  Eigen::Matrix4f camara_to_ref_for_culling_;
  Eigen::Matrix4f ref_to_camera_pose_;

  cv::Mat rotation_vec_;
  cv::Mat transition_vec_;

  float horizontal_fov_;
  float vertical_fov_;
  float near_plane_distance_; 
  float far_plane_distance_;
  

  cv::Mat distance_image_;
  cv::Mat point3d_image_;


  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_map_;

  double object_height_;

  std::queue<cv::Point3f> car_poses_;

  bool is_fisheye_;

private:

  bool GetGroundZ(const cv::Point2f& ball_center, float& ground_z);


 };
} // namespaceobject_location
}// namespaceobject_location
