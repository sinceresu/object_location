#ifndef PERSONPOSE_H
#define PERSONPOSE_H

#include "API.h"
#include <vector>
#include <string>
#include <iostream>
#include <numeric>
#include <algorithm>
#include "opencv2/opencv.hpp"

using namespace std;

class API PersonPose
{
public:
    PersonPose();
    ~PersonPose();
    
    std::vector<float> DoInference(cv::Mat img);
    int InitEngine(string &strModelDir);
    int H = 320;
    int W = 160;

private:
	PersonPose(const PersonPose &);
	const PersonPose &operator =(const PersonPose &);
	class Impl;
	Impl *_impl;
};


#endif
