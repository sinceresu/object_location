/**************************************************************************

Copyright: YiDa

Version: 1.10

Author: LiXingren

Date: 2022-05-18

Description: Input person keypoints.
             Output angle vertical with the surface of photo.
             2022-05-18: First edited.

**************************************************************************/
#include <iostream>
#include <fstream>
#include <cassert>
#include <string>
#include <string.h>

#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>

#include <exception>
#include <vector>
#include <stdint.h>
#include <math.h>

#include "person_angle/person_angle.h"

void GetFileNames(std::string path, std::vector<std::string> &filenames, std::string strFormat)
{
    DIR *pDir;
    struct dirent *ptr;
    if (!(pDir = opendir(path.c_str())))
        return;
    while ((ptr = readdir(pDir)) != 0)
    {
        if (strcmp(ptr->d_name, ".") != 0 && strcmp(ptr->d_name, "..") != 0)
        {
            std::string strFileName = ptr->d_name;
            int pos2 = strFileName.find('.');
            if (pos2 <= 0)
            {
                continue;
            }
            std::string strSuffix = strFileName.substr(pos2);
            if (strSuffix != strFormat)
            {
                continue;
            }
            filenames.push_back(path + "/" + strFileName);
        }
    }
    closedir(pDir);
}

int main(int argc, char **argv)
{
    try
    {
        PersonAngleLib::PersonAngle personAngle;
        std::ifstream infile;

        
        std::string strTestTxtDirPath = "/home/lxr/workdir/src/yd_person_angle/test_data";
        std::vector<std::string> strTestTxtPathList;
        std::string strFormat = ".txt";
        GetFileNames(strTestTxtDirPath, strTestTxtPathList, strFormat);
        int iTxtCnt = strTestTxtPathList.size();
        for (int i = 0; i < iTxtCnt; i++)
        {
            infile.open(strTestTxtPathList[i].data()); //将文件流对象与文件连接起来
            assert(infile.is_open());                  //若失败,则输出错误消息,并终止程序运行

            std::string line;

            std::vector<cv::Point2i> iKeypointList;
            while (getline(infile, line))
            {
                std::istringstream iss(line); 
                int x, y;
                // int key_frame;
                if (!(iss >> x >> y )) {
                    break;  
                }  
                iKeypointList.emplace_back(x, y);
            }
            infile.close(); //关闭文件输入流
            cv::Vec3f orientation;
            int result = personAngle.ComputeOrientation(iKeypointList, orientation);
            std::cout << "orientation:" << orientation << std::endl;
        }
    }
    catch (std::exception &e)
    {
        std::cout << "Standard exception:" << e.what() << std::endl;
    }

    return 0;
}