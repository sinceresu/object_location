#pragma once

#include <vector>
#include <memory>
#include <stdint.h>

#include <opencv2/opencv.hpp>


namespace person_localize2d{
  enum PersonJointOrder {
    HEAD = 0,
    NECK, 
    LEFT_SHOULDER,
    RIGHT_SHOULDER,
    LEFT_WAIST,
    RIGHT_WAIST,
    LEFT_KNEE,
    RIGHT_KNEE,
    LEFT_ANKLE,
    RIGHT_ANKLE,
    LEFT_HEEL,
    RIGHT_HEEL,
    LEFT_TOE,
    RIGHT_TOE,
    LEFT_SOLES,
    RIGHT_SOLES,
    TOTAL_JOINTS,

  };


class PersonLocalize2dInterface
{
public:
  virtual ~PersonLocalize2dInterface(){};

  /******************************************************************************
  * \fn PersonLocalize2dInterface.Localize
  * Create Time: 2022/05/25
  * Description: -
  *   开始进行着色建图
  *
  * \param persion_joints 输入参数
  *   关节数组，关节排列顺序由PersonJointOrder定义
  *   
  * \param position 输出参数
  * 	 行人在地面的投影点像素坐标
  * 
  * \return
  * 		错误码
  *
  * \note 着色输出的map名与输入map名一致。
  *******************************************************************************/

  virtual  int  Localize(const std::vector<cv::Point2i> & persion_joints,  const cv::Rect & person_box, cv::Point2i& position) = 0 ;


};
} // namespace person_localize2d

