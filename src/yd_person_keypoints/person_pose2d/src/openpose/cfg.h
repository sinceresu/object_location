#pragma once
#include <iostream>
#include <string>
#include <cstring>
using namespace std;

struct CFG_J
{
    string key;//索引
    string value;//值
    CFG_J *next;//下个结点
};
class ConfigFile
{
private:
    string file_name;//文件名字
    CFG_J * head;//头指针
    int cfg_line;//配置行数
    int createHead();//创建一个链表头指针
    int freeJoin();//释放链表的节点
    int inputFile();//内存配置同步配置到文件
    int joinHead(string key, string value);//将某个配置加入到链表中
public:
    ConfigFile(string file_name);//构造函数
    ~ConfigFile();//析构函数
    int getLines();//获取配置数量
    int setCFG(string key, string value);//设置一个配置
    string getCFG(string key);//从内存获取某个配置的值
    int getCFG();//从文件获取所有的配置 加载入内存链表
    void printCfg();//打印配置链表内容
};
