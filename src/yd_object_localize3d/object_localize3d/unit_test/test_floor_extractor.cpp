#include <algorithm>
#include <fstream>
#include <iostream>

#include <opencv2/opencv.hpp>
#include<pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>

#include "ros/ros.h"
#include "ros/time.h"
#include "rosbag/bag.h"
#include "rosbag/view.h"

#include "gflags/gflags.h"
#include "glog/logging.h"


#include "src/floor_extractor.h"


DEFINE_string(input_pcl, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(output_pcl, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");
DEFINE_double(sensor_height, 435, "[OPTIONAL] Probability threshold for detected objects");
DEFINE_double(height_clip_range, 437, "[OPTIONAL] IOU threshold for bounding box candidates");
DEFINE_bool(use_normal_filtering, false, "[OPTIONAL] IOU threshold for bounding box candidates");
DEFINE_bool(extract_only_floor, false, "[OPTIONAL] IOU threshold for bounding box candidates");
DEFINE_bool(use_upsample_filtering, false, "[OPTIONAL] IOU threshold for bounding box candidates");

using namespace std;

namespace object_location {
namespace  object_localize3d{

void Run( ) {
  FloorExtractor floor_extractor;
  FloorExtractor::FloorExtractParam param;
  param.sensor_height = FLAGS_sensor_height;
  param.height_clip_range = FLAGS_height_clip_range;
  param.use_normal_filtering = FLAGS_use_normal_filtering;
  param.extract_only_floor = FLAGS_extract_only_floor;
  param.use_upsample_filtering = FLAGS_use_upsample_filtering;

  param.normal_filter_thresh = 10;

  floor_extractor.SetParam(param);

  PointCloud::Ptr input_pcl_;

  input_pcl_.reset(new PointCloud() );

  pcl::io::loadPCDFile<PointType>(FLAGS_input_pcl, *input_pcl_);

  PointCloud::Ptr output_pcl_;
  output_pcl_.reset(new PointCloud() );

  floor_extractor.Extract(input_pcl_, output_pcl_);

  pcl::io::savePCDFileBinary<PointType>(FLAGS_output_pcl, *output_pcl_);

}
void Test(int argc, char** argv) {

    Run();

}

}
}

int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  CHECK(!FLAGS_input_pcl.empty())
      << "-configuration_directory is missing.";

  CHECK(!FLAGS_output_pcl.empty())
      << "-map_configuration_basename is missing.";


  object_location::object_localize3d::Test(argc, argv);

}
