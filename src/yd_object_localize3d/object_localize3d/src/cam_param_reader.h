#pragma once
#include <string>
#include <deque>
#include <set>

#include <Eigen/Core>
#include <opencv2/opencv.hpp>

namespace object_location
{

  namespace object_localize3d
  {

    class CamParamReader
    {
    public:
      explicit CamParamReader();
      virtual ~CamParamReader();

      CamParamReader(const CamParamReader &) = delete;
      CamParamReader &operator=(const CamParamReader &) = delete;

      virtual int LoadCalibrationFile(const std::string &_calibration_file);
      virtual int SetRotations(const std::vector<double> &_rads);

      cv::Mat GetIntrinsic()
      {
        return intrinsic_mat_;
      };
      cv::Mat GetDistortion()
      {
        return distortion_coeffs_;
      };
      cv::Size GetImageSize()
      {
        return image_size_;
      };
      Eigen::Matrix4d GetExtrinsic()
      {
        return extrinsic_mat_;
      };

    protected:
      cv::Mat intrinsic_mat_;
      cv::Mat distortion_coeffs_;
      cv::Size image_size_;
      Eigen::Matrix4d extrinsic_mat_;

    private:
      virtual int LoadIntrinsic(cv::FileStorage _fs);
      virtual int LoadExtrinsic(cv::FileStorage _fs);
    };

  }

}
