#pragma once

namespace object_location {
namespace common {

enum RawJointOrder{
  HEAD = 0,
  NECK = 1, 

  RIGHT_SHOULDER,
  RIGHT_ELBOW,
  RIGHT_WRIST,

  LEFT_SHOULDER,
  LEFT_ELBOW,
  LEFT_WRIST,

  TAIL,

  RIGHT_WAIST,
  RIGHT_KNEE,
  RIGHT_ANKLE,

  LEFT_WAIST,
  LEFT_KNEE,
  LEFT_ANKLE,


  RIGHT_EYE,
  LEFT_EYE,
  RIGHT_EAR,
  LEFT_EAR,

  LEFT_SOLES,
  LEFT_TOE,
  LEFT_HEEL,

  RIGHT_SOLES,
  RIGHT_TOE,
  RIGHT_HEEL,


  TOTAL_JOINTS,

};


}

}
