/*
 * @Description: In User Settings Edit
 * @Author: zerollzeng
 * @Date: 2019-08-23 11:55:03
 * @LastEditTime: 2020-05-22 11:40:34
 * @LastEditors: zerollzeng
 */

#include "PluginFactory.h"
#include "PReLUPlugin/PReLUPlugin.h"
#include "UpSamplePlugin/UpSamplePlugin.hpp"
#include "spdlog/spdlog.h"
#include <algorithm>
#include <cassert>

PluginFactoryCaffe::PluginFactoryCaffe(TrtPluginParams params) {
    spdlog::info("create plugin factory");

    mUpsampleScale = params.upsampleScale;
    spdlog::info("upsample params: scale: {}",mUpsampleScale);
}

//返回layer name是否是prelu，upsample，yolo-det其中一个，如果是，返回true，否则返回false
bool PluginFactoryCaffe::isPluginV2(const char* layerName) 
{
    std::string strName{layerName};
    std::transform(strName.begin(), strName.end(), strName.begin(), ::tolower);
    return (strName.find("prelu") != std::string::npos || strName.find("upsample") != std::string::npos);
}

//只有prelu，upsample，yolo-det其中一个，才返回非nullptr的结果
IPluginV2* PluginFactoryCaffe::createPlugin(const char *layerName, const Weights* weights, int nbWeights, const char* libNamespace) 
{
    assert(isPluginV2(layerName));

    std::string strName{layerName};
    std::transform(strName.begin(), strName.end(), strName.begin(), ::tolower);

    if (strName.find("prelu") != std::string::npos) {
        // std::cout << "nbWeight: " << nbWeights << std::endl;
        // std::cout << "weights.count: " << weights->count << std::endl;
        return (IPluginV2*)(new PReLUPlugin(weights, nbWeights));
    } 
    else if(strName.find("upsample") != std::string::npos) {
        return (IPluginV2*)(new UpSamplePlugin(mUpsampleScale));
    }
    else
    {
        std::cout << "warning : " << layerName << std::endl;
        assert(0);
        return nullptr;
    }
}


