#include "person2d_localizer.h"

#include <algorithm>
#include <math.h>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/linestring.hpp>
#include <boost/geometry/geometries/point_xy.hpp>


#include "gflags/gflags.h"
#include "glog/logging.h"

#include  "person2d_localizer.h"
#include "common.h"

#include "common/err_code.h"

// #define OUTPUT_PCL_FILES

using namespace std;
using namespace cv;
namespace  bg =  boost::geometry ;
 typedef bg::model::d2::point_xy<double> point_2d;
typedef bg::model::linestring<point_2d> linestring_2d;
static float  KFootLiftAngleThreshold = 60.f ;
static float  kKneeLengthDisparityFactor = 1.5f ;
static float  kWaistLengthDisparityFactor = 1.8f ;

namespace person_localize2d{
static float KLegToFootRatio = 0.2f;
static float KAnkleToHeelRatio = 1.0f;
static float KUpperBodyToLegRatio = 1.3f;
static float KHeadToBodyRatio = 10.f;

static int kAnkleHeightPixels = 8;
static int kMinShoeThicknessPixels =2;
static int kMaxShoeThicknessPixels =20;

namespace {
  // line cross o1, pi intersect with line cross o2, p26
bool intersection(Point2i o1, Point2i p1, Point2i o2, Point2i p2,
                      Point2i &r)
{
    Point2i x = o2 - o1;
    Point2i d1 = p1 - o1;
    Point2i d2 = p2 - o2;

    float cross = d1.x*d2.y - d1.y*d2.x;
    if (abs(cross) < /*EPS*/1e-8)
        return false;

    double t1 = (x.x * d2.y - x.y * d2.x)/cross;
    r = o1 + d1 * t1;
    return true;
}

float getDist_P2L(cv::Point2i pointP, cv::Point2i  pointA, cv::Point2i  pointB)
{
    //求直线方程
    int A = 0, B = 0, C = 0;
    A = pointA.y - pointB.y;
    B = pointB.x - pointA.x;
    C = pointA.x*pointB.y - pointA.y*pointB.x;
    //代入点到直线距离公式
    float distance = 0;
    distance = ((float)abs(A*pointP.x + B*pointP.y + C)) / ((float)sqrtf(A*A + B*B));
    return distance;
}



bool GetMiddleLineOfBody(const Point2i & head, const Point2i & left_shoulder, const Point2i & right_shoulder, const Point2i & left_waist, const Point2i & right_waist, Point2i &point_of_middle_line)  {
  if ( left_waist.x != 0  &&  right_waist.x != 0 ) {
    point_of_middle_line = ( left_waist + right_waist)  / 2;
    return true;
  }
    
  if ( left_shoulder.x != 0  &&  right_shoulder.x != 0 ) {
    point_of_middle_line = ( left_shoulder + right_shoulder)  / 2;
    return true;
  }
      
  if ( head.x != 0 ) {
    point_of_middle_line = head ;
    return true;
  }


  if ( left_waist.x != 0 ) {
    point_of_middle_line =left_waist;
    return true;
  }

  if ( right_waist.x != 0 ) {
    point_of_middle_line = right_waist;
    return true;
  }

  if ( left_shoulder.x != 0 ) {
    point_of_middle_line =left_shoulder;
    return true;
  }

  if ( right_shoulder.x != 0 ) {
    point_of_middle_line = right_shoulder;
    return true;
  }

  return false;
}

bool IsOneFootLifted(const Point2i & left_ankle, const Point2i & right_ankle)  {
  assert(left_ankle.x != 0 || right_ankle.x != 0);

  auto feet_vector = right_ankle - left_ankle;

  auto angle = abs(atan(float(feet_vector.y)/feet_vector.x) * 180 / M_PI);
  return angle > KFootLiftAngleThreshold;
}

bool IsFootWarped(const Point2i & heel, const Point2i & toe)  {
  assert(heel.x != 0 || toe.x != 0);

  auto feet_vector = heel - toe;
  auto angle = abs(atan(feet_vector.y/feet_vector.x) * 180 / M_PI);
  return angle > KFootLiftAngleThreshold;
}


bool GetCenterBetweenJoints(const Point2i & center_point, const Point2i & left_joint, const Point2i & right_joint, Point2i& center_position)  {
  assert(left_joint.x != 0 || right_joint.x != 0);

  if ( left_joint.x != 0  &&  right_joint.x != 0 ) {
    // if one foot lifted than only use the lower ankle to get ankle height;
    if (!IsOneFootLifted(left_joint, right_joint)) {
      intersection(center_point, Point2i(center_point.x , center_point.y + 10), left_joint, right_joint, center_position);
      return true;
    }
    center_position = left_joint.y > right_joint.y ? left_joint : right_joint;
    return true;
  }

  //only  one ankle
  if ( left_joint.x != 0 ) {
    center_position.x = center_point.x;
    center_position.y = left_joint.y;
    return true;
  }

  center_position.x = center_point.x;
  center_position.y = right_joint.y;
  return true;

}



bool GetCenterBetweenJoints(const Point2i & head, const Point2i & left_shoulder, const Point2i & right_shoulder, const Point2i & left_waist, const Point2i & right_waist, const Point2i & left_joint, const Point2i & right_joint, Point2i& center_position)  {

  Point2i point_of_middle_line;
  if (GetMiddleLineOfBody(head, left_shoulder, right_shoulder, left_waist, right_waist, point_of_middle_line)) {
    return GetCenterBetweenJoints(point_of_middle_line, left_joint, right_joint, center_position);
  }
  // no center point
  // two ankle
  if ( left_waist.x != 0  &&  right_waist.x != 0 ) {
    center_position = (left_joint + right_joint) / 2;
    return true;
  }
  
  // one ankle
  center_position = left_joint.x != 0 ?  left_joint : right_joint;
  return true;
}

int GetFootHeight( const Point2i & left_knee, const Point2i & right_knee, const Point2i & left_ankle, const Point2i & right_ankle)  {
  int foot_height = kAnkleHeightPixels;
  //not all ankle and knee at both sides
  if ((left_knee.x == 0  || left_ankle.x == 0 ) && (right_knee.x == 0  || right_ankle.x == 0 ) )
    return foot_height;

   float left_foot_length  = -1.f;
  if (left_knee.x != 0 && left_ankle.x != 0 ) {
      float leg_length = norm(left_knee - left_ankle);
      left_foot_length = leg_length * KLegToFootRatio;
  }

   float right_foot_length  = -1.f;
  if (right_knee.x != 0  && right_ankle.x != 0 ) {
    float leg_length = norm(right_knee - right_ankle);
    right_foot_length = leg_length * KLegToFootRatio;
 }

// select longer or at exist
  foot_height = round(max(left_foot_length, right_foot_length));

  return foot_height;
}

float GetHeelHeight( const Point2i & left_ankle, const Point2i & right_ankle, const Point2i & left_heel, const Point2i & right_heel)  {
  //not all ankle and knee at both sides
  int heel_height = kMinShoeThicknessPixels;

  if ((left_ankle.x == 0  || left_heel.x == 0 ) && (right_ankle.x == 0  || right_heel.x == 0 ) )
    return heel_height;

   float left_foot_length  = -1.f;
  if (left_ankle.x != 0 && left_ankle.x != 0 ) {
      float ankle_length = norm(left_ankle - left_heel);
      left_foot_length = ankle_length * KAnkleToHeelRatio;
  }

   float right_foot_length  = -1.f;
  if (right_heel.x != 0  && right_heel.x != 0 ) {
    float ankle_length = norm(right_ankle - right_heel);
    right_foot_length = ankle_length * KAnkleToHeelRatio;
 }

// select longer or at exist
  heel_height = max(left_foot_length, right_foot_length);
  heel_height = max(kMinShoeThicknessPixels, heel_height);
  heel_height =  min(kMaxShoeThicknessPixels, heel_height);

  return heel_height;
}


bool CheckHeelsValid( const Point2i & left_waist, const Point2i & right_waist, const Point2i & left_knee, const Point2i & right_knee, const Point2i & left_heel, const Point2i & right_heel) {
  if (left_knee.x != 0 && right_knee.x != 0 ) {
    auto left_length = norm(left_knee - left_heel);
    auto right_length = norm(right_knee - right_heel);
    // valid if the difference between length of  two legs is not too much.
    if (left_length < right_length *kKneeLengthDisparityFactor && left_length >  right_length / kKneeLengthDisparityFactor ) {
      return true;
    }
    return false;
  }

  if (left_waist.x != 0 && right_waist.x != 0 ) {
    auto left_length = norm(left_waist - left_heel);
    auto right_length = norm(right_waist - right_heel);

    // valid if the difference between length of  two legs is not too much.
    if (left_length < right_length *kWaistLengthDisparityFactor && left_length >  right_length / kWaistLengthDisparityFactor ) {
      return true;
    }
    return false;
  }

  return false;
}

bool LocalizeWithHeels(const Point2i & head, const Point2i & left_shoulder, const Point2i & right_shoulder, const Point2i & left_waist, const Point2i & right_waist, const Point2i & left_knee, const Point2i & right_knee, const Point2i & left_ankle, const Point2i & right_ankle, const Point2i & left_heel, const Point2i & right_heel, Point2i& center_position)  {
    // strictest
  if(left_heel.x == 0 || right_heel.x == 0) {
    return false;
  }
 if (!CheckHeelsValid(left_waist, right_waist, left_knee, right_knee, left_heel, right_heel)) {
   return false;
 }


if (GetCenterBetweenJoints(head, left_shoulder, right_shoulder, left_waist, right_waist, left_heel, right_heel, center_position)) {
   // add thickness of shoes ;
     float heel_height =GetHeelHeight(left_ankle, right_ankle, left_heel, right_heel);
    center_position.y =  round(center_position.y + heel_height);
    return true;
  }
  
  return false;
}

bool LocalizeWithAnkles(const Point2i & head, const Point2i & left_shoulder, const Point2i & right_shoulder, const Point2i & left_waist, const Point2i & right_waist, const Point2i & left_knee, const Point2i & right_knee, const Point2i & left_ankle, const Point2i & right_ankle, Point2i& center_position)  {

  if (left_ankle.x == 0 && right_ankle.x == 0){
    return false;
  }

  
  Point2i center_between_ankles;

  if (GetCenterBetweenJoints(head, left_shoulder, right_shoulder, left_waist, right_waist, left_ankle, right_ankle, center_between_ankles)) {
    int foot_height = GetFootHeight(left_knee, right_knee,   left_ankle, right_ankle);
    center_position.x = center_between_ankles.x;
    center_position.y = center_between_ankles.y + foot_height;

    return true;

  }
  
  return false;
}

bool GetBodyHeight(  const Point2i & left_shoulder, const Point2i & right_shoulder, const Point2i & left_waist, const Point2i & right_waist,  float& body_height)  {

    
 float body_left_height = -1.f;
// if left is invalid, then select right;
  if (left_shoulder.x != 0  && left_waist.x != 0) {
    body_left_height = abs(left_shoulder.y - left_waist.y);
  }

  float body_right_height = -1.f;
  if (right_shoulder.x  != 0 && right_waist.x  != 0) {
    body_right_height = abs(right_shoulder.y - right_waist.y);
 }

  //左，右边至少有一边肩膀和腰部同时存在
  if (body_left_height <= 0.f  && body_right_height <= 0.f )
    return false;

  // 如果左右边关节都存在，则取平均高度  
  if (body_left_height > 0.f  & body_right_height > 0.f ) {
    body_height = (body_left_height + body_right_height) / 2.f;
    return true;
  }
  
  body_height = max(body_left_height, body_right_height);

  return true;
}
bool GetHeadHeight(const Point2i & head, const Point2i & neck, const Point2i & left_shoulder, const Point2i & right_shoulder, float& head_height)  {
  if (head.x == 0) 
    return false;
    
  if (neck.x != 0) {
    head_height = abs(neck.y - head.y);
    return true;
  }

  if (left_shoulder.x != 0  && right_shoulder.x != 0) {
    Point2f neck_center = (left_shoulder + right_shoulder) / 2.f;
    head_height = abs(neck_center.y - head.y);
    return true;
  }
  
  return false;
}


bool LocalizeWithUpperBody(const Point2i & head, const Point2i & left_shoulder, const Point2i & right_shoulder, const Point2i & left_waist, const Point2i & right_waist, const Rect & person_box,  Point2i& center_position)  {

  float body_height;
  if (!GetBodyHeight(left_shoulder, right_shoulder,  left_waist, right_waist, body_height) ) {
    return false;
  }

  Point2i waist_center;
  if (left_waist.x != 0 && right_waist.x != 0){
    waist_center = (left_waist + right_waist) / 2;
  } else if (left_waist.x != 0) {
    waist_center = left_waist;
  } else {
    waist_center = right_waist;
  }

  center_position.x = waist_center.x;
  center_position.y = waist_center.y + body_height * KUpperBodyToLegRatio;

  center_position.y = min(center_position.y,  person_box.y + person_box.height );

  return true;
}

bool LocalizeWithHead(const Point2i & head, const Point2i & neck, const Point2i & left_shoulder, const Point2i & right_shoulder, const Rect & person_box, Point2i& center_position)  {

  float head_height;
  if (!GetHeadHeight(head, neck, left_shoulder, right_shoulder, head_height) ) {
    return false;
  }

  center_position.x = head.x;
  center_position.y = head.y + round(head_height * KHeadToBodyRatio);

  // center_position.y = min(center_position.y,  person_box.y + person_box.height);

  return true;
}

}

Person2dLocalizer::Person2dLocalizer()
: initialized_(false)
{
}


int Person2dLocalizer:: Localize(const vector<cv::Point2i> & persion_joints,  const cv::Rect & person_box, Point2i& position)  {

  auto head = persion_joints[(int)PersonJointOrder::HEAD];
  
  auto neck = persion_joints[(int)PersonJointOrder::NECK];

  auto left_shoulder = persion_joints[(int)PersonJointOrder::LEFT_SHOULDER];
  auto right_shoulder =  persion_joints[(int)PersonJointOrder::RIGHT_SHOULDER];

  auto left_waist = persion_joints[(int)PersonJointOrder::LEFT_WAIST];
  auto right_waist =persion_joints[(int)PersonJointOrder::RIGHT_WAIST];

  auto left_knee =persion_joints[(int)PersonJointOrder::LEFT_KNEE];
  auto right_knee =persion_joints[(int)PersonJointOrder::RIGHT_KNEE];

  auto left_ankle =persion_joints[(int)PersonJointOrder::LEFT_ANKLE];
  auto right_ankle =persion_joints[(int)PersonJointOrder::RIGHT_ANKLE];

  auto left_heel =persion_joints[(int)PersonJointOrder::LEFT_HEEL];
  auto right_heel =persion_joints[(int)PersonJointOrder::RIGHT_HEEL];

  auto left_toe =persion_joints[(int)PersonJointOrder::LEFT_TOE];
  auto right_toe =persion_joints[(int)PersonJointOrder::RIGHT_TOE];

  if (left_toe.x != 0 && (left_heel.x == 0 || left_heel.y < left_toe.y)) {
    if (IsFootWarped(left_heel, left_toe))
      left_heel = left_toe;
  }
  if (right_toe.x != 0 && (right_heel.x == 0 || right_heel.y < right_toe.y)) {
    if (IsFootWarped(right_heel, right_toe))
     right_heel = right_toe;
  }

  if (LocalizeWithHeels(head, left_shoulder, right_shoulder, left_waist, right_waist, left_knee, right_knee, left_ankle, right_ankle, left_heel, right_heel, position)) {
    return ERRCODE_OK;
  }

  if (LocalizeWithAnkles(head, left_shoulder, right_shoulder, left_waist, right_waist, left_knee, right_knee, left_ankle, right_ankle, position)) {
    return ERRCODE_OK;
  }

  if (LocalizeWithUpperBody(head, left_shoulder, right_shoulder, left_waist, right_waist, person_box, position)) {
    return ERRCODE_OK;
  }

  if (LocalizeWithHead(head, neck, left_shoulder, right_shoulder, person_box, position)) {
    return ERRCODE_OK;
  }


  return ERRCODE_FAILED;
}



} // namespace person_localize2d
