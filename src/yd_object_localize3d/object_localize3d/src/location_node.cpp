#include "location_node.h"
#include <limits>
#include <iostream>
#include <fstream>
#include <sstream> 
#include <thread> 

#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PolygonStamped.h"
#include "sensor_msgs/point_cloud_conversion.h"
#include "geometry_msgs/PoseArray.h"

#include <Eigen/Core>
#include <opencv2/core/core.hpp>
#include <opencv2/core/eigen.hpp>
#include <pcl/io/pcd_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl_conversions/pcl_conversions.h>
#include "glog/logging.h"
#include "tf2/LinearMath/Quaternion.h"
#include "tf2/LinearMath/Matrix3x3.h"
#include "object_localizer.h"
#include "object_localizer_factory.h"

#include "person_localize2d/libperson_localize2d.h"
#include "person_localize2d/person_localize2d_interface.h"

#include "person_angle/person_angle.h"


#include "common/err_code.h"
#include "common/person_joints.h"

#include "floor_extractor.h"

#include "cam_param_reader.h"
#include "rotate_cam_param_reader.h"


using namespace std;
using namespace cv;

using namespace person_localize2d;
using namespace PersonAngleLib;

namespace object_location {
namespace  object_localize3d{

constexpr int kInfiniteSubscriberQueueSize = 0;
constexpr int kLatestOnlyPublisherQueueSize = 1;
constexpr int kExtendPixels = 20;

namespace {
// Subscribes to the 'topic' for 'trajectory_id' using the 'node_handle' and
// calls 'handler' on the 'node' to handle messages. Returns the subscriber.
template <typename MessageType>
::ros::Subscriber SubscribeWithHandler(
    void (LocationNode::*handler)(const std::string&,
                          const typename MessageType::ConstPtr&),
                        const std::string& topic,
    ::ros::NodeHandle* const node_handle, LocationNode* const node) {
  return node_handle->subscribe<MessageType>(
      topic, kInfiniteSubscriberQueueSize,
      boost::function<void(const typename MessageType::ConstPtr&)>(
          [node, handler,
           topic](const typename MessageType::ConstPtr& msg) {
            (node->*handler)(topic, msg);
          }));
}


vector<Point2i> ConvertToPersonJoint(const geometry_msgs::Polygon::_points_type& raw_joints) {
  vector<Point2i> person_joints((int)PersonJointOrder::TOTAL_JOINTS);

  auto raw_joint = raw_joints[(int)RawJointOrder::HEAD];
  person_joints[(int)PersonJointOrder::HEAD] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::NECK];
  person_joints[(int)PersonJointOrder::NECK] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::LEFT_SHOULDER];
  person_joints[(int)PersonJointOrder::LEFT_SHOULDER] = Point2f((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::RIGHT_SHOULDER];
  person_joints[(int)PersonJointOrder::RIGHT_SHOULDER] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::LEFT_WAIST];
  person_joints[(int)PersonJointOrder::LEFT_WAIST] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::RIGHT_WAIST];
  person_joints[(int)PersonJointOrder::RIGHT_WAIST] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::LEFT_KNEE];
  person_joints[(int)PersonJointOrder::LEFT_KNEE] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::RIGHT_KNEE];
  person_joints[(int)PersonJointOrder::RIGHT_KNEE] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::LEFT_ANKLE];
  person_joints[(int)PersonJointOrder::LEFT_ANKLE] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::RIGHT_ANKLE];
  person_joints[(int)PersonJointOrder::RIGHT_ANKLE] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::LEFT_HEEL];
  person_joints[(int)PersonJointOrder::LEFT_HEEL] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  raw_joint = raw_joints[(int)RawJointOrder::RIGHT_HEEL];
  person_joints[(int)PersonJointOrder::RIGHT_HEEL] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  // raw_joint = raw_joints[(int)RawJointOrder::LEFT_TOE];
  // person_joints[(int)PersonJointOrder::LEFT_TOE] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  // raw_joint = raw_joints[(int)RawJointOrder::RIGHT_TOE];
  // person_joints[(int)PersonJointOrder::RIGHT_TOE] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  // raw_joint = raw_joints[(int)RawJointOrder::LEFT_SOLES];
  // person_joints[(int)PersonJointOrder::LEFT_SOLES] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  // raw_joint = raw_joints[(int)RawJointOrder::RIGHT_SOLES];
  // person_joints[(int)PersonJointOrder::RIGHT_SOLES] = Point2i((int)round(raw_joint.x), (int)round(raw_joint.y)) ;

  return person_joints;
}

// vector<int16_t> ConvertToPersonJointOfPersonAngle(const geometry_msgs::Polygon::_points_type& raw_joints) {
//   vector<int16_t> person_joints;
//   for (size_t i  = 0; i <  raw_joints.size(); i++) {
//     person_joints.push_back((int16_t)round(raw_joints[i].x));
//     person_joints.push_back((int16_t)round(raw_joints[i].y));
//   }
//   return person_joints;
// }

vector<Point2i> ConvertToPersonJointOfPersonAngle(const geometry_msgs::Polygon::_points_type& raw_joints) {
  vector<Point2i> person_joints;
  for (size_t i  = 0; i <  raw_joints.size(); i++) {
    person_joints.push_back(Point2i(round(raw_joints[i].x),round(raw_joints[i].y) ));
  }
  return person_joints;
}

Eigen::Quaternionf GetOrientationByPoints(const cv::Point3f& origin, const cv::Point3f& target) {

  Eigen::Vector3f x_direction ( target.x - origin.x, target.y - origin.y , 0.f);
  x_direction.normalize();

   Eigen::Vector3f z_direction ( 0.f, 0.f , 1.f);

   Eigen::Vector3f  y_direction = z_direction.cross(x_direction);

  Eigen::Matrix3f orientation_mat;
  orientation_mat.block<3,1>(0, 0) = x_direction;
  orientation_mat.block<3,1>(0, 1) = y_direction;
  orientation_mat.block<3,1>(0, 2) = z_direction;

  Eigen::Quaternionf orientation(orientation_mat);
  return orientation.normalized();
}

Eigen::Quaternionf GetOrientationByVector(const cv::Vec3f& orientation_vec) {

  Eigen::Vector3f x_direction ( orientation_vec[0], orientation_vec[1] , 0.f);
  x_direction.normalize();

   Eigen::Vector3f z_direction ( 0.f, 0.f , 1.f);

   Eigen::Vector3f  y_direction = z_direction.cross(x_direction);

  Eigen::Matrix3f orientation_mat;
  orientation_mat.block<3,1>(0, 0) = x_direction;
  orientation_mat.block<3,1>(0, 1) = y_direction;
  orientation_mat.block<3,1>(0, 2) = z_direction;

  Eigen::Quaternionf orientation(orientation_mat);
  return orientation.normalized();
}


}

LocationNode::LocationNode(const LocationNodeOptions& node_options) : node_options_(node_options)
{
  LaunchPublishs();

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_map= pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>() );
  pcl::io::loadPCDFile(node_options_.cloud_map_filepath, *cloud_map);

  if (node_options_.extract_floor) {
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr floor_map= pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>() );
        auto floor_extractor = make_shared<FloorExtractor>();
        FloorExtractor::FloorExtractParam param;
        param.sensor_height = node_options_.sensor_height;
        param.height_clip_range = node_options_.height_clip_range;
        param.use_normal_filtering = node_options_.use_normal_filtering;
        param.extract_only_floor = node_options_.extract_only_floor;
        param.use_upsample_filtering = node_options_.use_upsample_filtering;

        floor_extractor->SetParam(param);
        if (ERRCODE_OK == floor_extractor->Extract(cloud_map, floor_map)) {
          *cloud_map = *floor_map;
        }
    }
  }
  // rviz
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr rgb_cloud_map = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>());
  pcl::io::loadPCDFile(node_options_.cloud_map_filepath, *rgb_cloud_map);
  float leaf_size = 0.01;
  boost::shared_ptr<pcl::ApproximateVoxelGrid<pcl::PointXYZRGB>> voxel_grid(new pcl::ApproximateVoxelGrid<pcl::PointXYZRGB>());
  voxel_grid->setLeafSize(leaf_size, leaf_size, leaf_size);
  voxel_grid->setInputCloud(rgb_cloud_map);
  pcl::PointCloud<pcl::PointXYZRGB> global_filtered;
  voxel_grid->filter(global_filtered);
  //pub global map
  sensor_msgs::PointCloud2 global_map;
  pcl::toROSMsg(global_filtered, global_map);
  global_map.header.frame_id = "map";
  global_map_publisher_.publish(global_map);
  LOG(INFO) << "publish global map!";
  // culling map
  Eigen::Matrix4d camara_to_map_pose;
  Eigen::Matrix4d extrinsic_mat;
  cv::Mat intrinsic_mat;
  cv::Mat distortion_coeff;
  cv::Size image_size;
  Eigen::Matrix4d camera_pose;
  cam_param_reader_ = node_options_.ptz_rotate ? std::make_shared<RotateCamParamReader>() : std::make_shared<CamParamReader>();
  if (cam_param_reader_->LoadCalibrationFile(node_options_.calibration_filepath) == 0)
  {
    std::vector<double> rads(2, 0.0);
    cam_param_reader_->SetRotations(rads);
    camara_to_map_pose = cam_param_reader_->GetExtrinsic();
    extrinsic_mat = cam_param_reader_->GetExtrinsic();
    intrinsic_mat = cam_param_reader_->GetIntrinsic();
    distortion_coeff = cam_param_reader_->GetDistortion();
    image_size = cam_param_reader_->GetImageSize();
    Eigen::Matrix4d cam2robot;
    cam2robot << 0, 0, 1, 0,
                0, -1, 0, 0,
                1, 0, 0, 0,
                0, 0, 0, 1;
    camera_pose = camara_to_map_pose * cam2robot; 
  }
  pcl::FrustumCulling<pcl::PointXYZRGB> fc;
  fc.setInputCloud(rgb_cloud_map);
  fc.setHorizontalFOV(node_options_.horizontal_fov);
  fc.setVerticalFOV(node_options_.vertical_fov);
  fc.setNearPlaneDistance(node_options_.near_plane_distance);
  fc.setFarPlaneDistance(node_options_.far_plane_distance);
  fc.setCameraPose(camera_pose.cast<float>());
  pcl::PointCloud <pcl::PointXYZRGB> camera_filtered;
  fc.filter(camera_filtered);
  //pub camera map
  sensor_msgs::PointCloud2 camera_map;
  pcl::toROSMsg(camera_filtered, camera_map);
  camera_map.header.frame_id = "map";
  camera_map_publisher_.publish(camera_map);
  LOG(INFO) << "publish camera map!";
  // --

  type_to_label_.clear();
  for (size_t i = 0;  i <  node_options.object_types.size(); i++) {
    int object_type = node_options.object_types[i];
    type_to_label_[object_type] = node_options.object_labels[i];

    pose_localizers_[object_type] = ObjectLocalizerFactory::CreateObjectLocalizer(node_options.object_labels[i]);
      // pose_localizer_ =std::make_shared<RobotLocalizer>();
    if (pose_localizers_[object_type] ->LoadCalibrationFile(node_options_.calibration_filepath, node_options_.ptz_rotate) != 0)
      return;
    
    pose_localizers_[object_type]->SetCullingParamters(node_options_.horizontal_fov, 
      node_options_.vertical_fov, 
      node_options_.near_plane_distance, 
      node_options_.far_plane_distance
    );
    pose_localizers_[object_type] ->SetParamters(node_options_.object_heights[i]); 

    pose_localizers_[object_type] ->SetCloudMap(cloud_map);

    if (node_options.object_labels[i] == "person") {
      person_object_type_ = object_type;
    }
  }

  person2d_localizer_ = CreatePerson2dLocalizer( );

  person_angle_ = make_shared<PersonAngle>();
  person_angle_ ->LoadCalibrationFile(node_options_.calibration_filepath);


  LaunchSubscribers();

}


LocationNode::~LocationNode() { }

void LocationNode::LaunchPublishs() {
  pose_position_publisher_ = node_handle_.advertise<::object_localize3d_msgs::ObjectPose>(node_options_.location_topic, kLatestOnlyPublisherQueueSize);

  global_map_publisher_ = node_handle_.advertise<sensor_msgs::PointCloud2>("/global/map", 1,true);
  camera_map_publisher_ = node_handle_.advertise<sensor_msgs::PointCloud2>("/camera/map", 1,true);
  obj_pose_publisher_ = node_handle_.advertise<geometry_msgs::PoseArray>("/object/pose", 1);
}

void LocationNode::LaunchSubscribers() {
  ptz_pose_subscriber_ = SubscribeWithHandler<nav_msgs::Odometry>(
        &LocationNode::PtzPoseCallback, node_options_.ptz_pose_topic, &node_handle_,
        this);
  poseKeypoints_subscriber_ = SubscribeWithHandler<person_pose2d_msgs::PersonPose2d>(
        &LocationNode::HandlePoseKeypointsMessage, node_options_.poseKeypoints_topic, &node_handle_,
        this);
}

void LocationNode::ptz_pose_update(const nav_msgs::Odometry::ConstPtr &msg) {
  for (std::map<int, std::shared_ptr<ObjectLocalizer>>::iterator it = pose_localizers_.begin(); it != pose_localizers_.end(); it++)
  {
    std::vector<double> rotations = {msg->pose.pose.position.x, msg->pose.pose.position.z};
    it->second->SetRotations(rotations);
  }
}

void LocationNode::PtzPoseCallback(const std::string &sensor_id,
                                  const nav_msgs::Odometry::ConstPtr &msg)
{
  ptz_pose_update(msg);
}


Vec3f LocationNode::GetPerson3dOrientation(const vector<Point2i> & person_joints, const Point2f& person_position2d, const Point3f& person_position3d){
  Vec3f person_orientation3d(0.f, 0.f, 1.0f);

  //first get orientation by upper body
  Vec3f person_orientation_in_cam;
  if ( person_angle_->ComputeOrientation(person_joints, person_orientation_in_cam)) {
     pose_localizers_[person_object_type_]->LocalizeOrientation(person_orientation_in_cam, person_orientation3d);
     return person_orientation3d;
  }

  //get orientation by toes
  Vec2f person_orientation2d_in_cam;
  if ( person_angle_->Compute2DOrientation(person_joints, person_orientation2d_in_cam)) {
    Point2f person_forward_point2d(person_position2d + Point2f(person_orientation2d_in_cam[0], person_orientation2d_in_cam[1]));

    cv::Point3f person_forward_point3d;
    if (ERRCODE_OK == pose_localizers_[person_object_type_]->LocalizePoint(person_forward_point2d, person_forward_point3d) ) {
      person_orientation3d =  Vec3f(person_forward_point3d.x - person_position3d.x, person_forward_point3d.y - person_position3d.y , 0.f);
    }
  }
  return person_orientation3d;
}

bool  LocationNode::GetPerson3dPose(const geometry_msgs::Polygon & key_points,::geometry_msgs::Pose & pose_msg) {

  auto person_poses = ConvertToPersonJoint(key_points.points);
  cv::Rect2i person_box(0, 0, 0, 0);

  // include object detection box
  if (key_points.points.size() == (int)RawJointOrder::TOTAL_JOINTS + 2)
  {
    static double dMExtendScale = 0.2;
    person_box.x = (int)round(key_points.points[ (int)RawJointOrder::TOTAL_JOINTS].x);
    person_box.y = (int)round(key_points.points[ (int)RawJointOrder::TOTAL_JOINTS].y);
    person_box.width = (int)round(key_points.points[ (int)RawJointOrder::TOTAL_JOINTS + 1].x -  person_box.x);
    person_box.height = (int)round(key_points.points[ (int)RawJointOrder::TOTAL_JOINTS + 1].y -  person_box.y);
    person_box.width = person_box.width  /  (1 + 2*dMExtendScale);
    person_box.height = person_box.height  /  (1 + 2*dMExtendScale);
    person_box.x = person_box.x +  person_box.width * dMExtendScale;
    person_box.y = person_box.y +  person_box.height * dMExtendScale;
}  

  Point2i person_position2d;
  int result =  person2d_localizer_->Localize(person_poses, person_box, person_position2d);
  if (ERRCODE_OK != result) {
    return false;
  }

  cv::Point3f person_position3d;
  if (0 != pose_localizers_[person_object_type_]->LocalizePoint(Point2f(person_position2d.x, person_position2d.y), person_position3d) ) {
    return false;
  }
  // auto person_poses_angle = ConvertToPersonJointOfPersonAngle(key_points.points);
  // float person_angle = person_angle_->ComputeVAngle(person_poses_angle);
  auto person_joints = ConvertToPersonJointOfPersonAngle(key_points.points);

// upwards as invalid direction.
  cv::Vec3f person_orientation3d = GetPerson3dOrientation(person_joints, person_position2d, person_position3d);

  auto orientation3d = GetOrientationByVector(person_orientation3d);

  ::geometry_msgs::Point position_msg;
  position_msg.x = person_position3d.x;
  position_msg.y = person_position3d.y;
  position_msg.z = person_position3d.z;

  ::geometry_msgs::Quaternion orientation_msg;
  orientation_msg.w = orientation3d.w();
  orientation_msg.x = orientation3d.x();
  orientation_msg.y = orientation3d.y();
  orientation_msg.z = orientation3d.z();

  pose_msg.position = position_msg;
  pose_msg.orientation = orientation_msg;

  return true;
}

void LocationNode::HandlePoseKeypointsMessage(const std::string& sensor_id,
                              const person_pose2d_msgs::PersonPose2d::ConstPtr& msg) {
  // LOG(INFO) << "HandlePoseKeypointsMessage.";
  // ROS_INFO_STREAM(*msg);
  object_localize3d_msgs::ObjectPose object_poses;
  object_poses.header.frame_id = msg->header.frame_id;
  object_poses.header.stamp =   msg->header.stamp;
  object_poses.cam_id =   msg->cam_id;

  for (size_t i = 0 ; i <msg->person_poses.size(); i++) {
    int object_id = msg->object_ids[i];
    int object_type = msg->object_types[i];
    //
    if ( object_type == person_object_type_) {
      if (msg->person_poses[i].points.size() < 27)
      {
ROS_INFO_STREAM("info type:" << object_type << " size:" << msg->person_poses[i].points.size());
         continue;
      }
      CHECK_GE(msg->person_poses[i].points.size(), (int)RawJointOrder::RIGHT_ANKLE + 1) << "the number of object region points is not 2!";

      ::geometry_msgs::Pose pose_msg;
      if (GetPerson3dPose(msg->person_poses[i], pose_msg)) {
        object_poses.object_poses.push_back(pose_msg);
        object_poses.object_ids.push_back(object_id);
        object_poses.object_types.push_back(object_type);
      }

      // std::ofstream out_path("/home/yd/Bin/person_data/position.txt", std::ios::out| std::ios::app | std::ios::binary);
      // if (out_path)
      // {
      //   out_path << "[" << msg->header.stamp << "]"
      //     << " person_id:" << object_id
      //     << " position:" << "[" << position.x << ", " << position.y<< ", " << position.z << "]" << std::endl;
      // }
      // out_path.close();
    } else { // other object
	    continue;
      CHECK_EQ(msg->person_poses[i].points.size(), 2) << "the number of object region points is not 2!";
      const auto& object_pose2d = msg->person_poses[i];
      float object_x = object_pose2d.points[0].x;
      float object_y = object_pose2d.points[0].y;
      float object_width = object_pose2d.points[1].x - object_x;
      float object_height = object_pose2d.points[1].y - object_y;

      cv::Rect2f object_region(object_x , object_y, object_width, object_height);

      cv::Point3f object_position3d;
      if (ERRCODE_OK == pose_localizers_[object_type]->Localize(object_region, object_position3d) ) {
        ::geometry_msgs::Pose pose_msg;
        ::geometry_msgs::Point position_msg;
        position_msg.x = object_position3d.x;
        position_msg.y = object_position3d.y;
        position_msg.z = object_position3d.z;
        pose_msg.position = position_msg;

        object_poses.object_poses.push_back(pose_msg);
        object_poses.object_ids.push_back(object_id);
        object_poses.object_types.push_back(object_type);   
      }


    }

  }
  
  pose_position_publisher_.publish(object_poses);

  // rviz
  geometry_msgs::PoseArray rviz_poses;
  rviz_poses.header.frame_id = "map";
  rviz_poses.header.stamp = ros::Time::now();
  for (const auto &obj_pose : object_poses.object_poses)
  {
    rviz_poses.poses.push_back(obj_pose);
  }
  obj_pose_publisher_.publish(rviz_poses);

}

}

}
