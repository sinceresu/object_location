#include <algorithm>
#include <fstream>
#include <iostream>

#include <opencv2/opencv.hpp>
#include<pcl/io/pcd_io.h>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "src/object_localizer.h"


DEFINE_string(calibration_filepath, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(cloud_map_filepath, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");


using namespace std;

namespace object_location {
namespace  object_localize3d{
namespace {


std::shared_ptr<ObjectLocalizer> BuildPoseLocalizer( 
                    const std::string& calibration_filepath) {


auto  localizer = std::make_shared<ObjectLocalizer>();
localizer->LoadCalibrationFile(calibration_filepath);
  return localizer;
}

void Run( ) {

  auto localizer = BuildPoseLocalizer(FLAGS_calibration_filepath);

    pcl::PointCloud<pcl::PointXYZ>::Ptr input_point_cloud = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>() );

    pcl::io::loadPCDFile(FLAGS_cloud_map_filepath, *input_point_cloud);

    localizer->SetCloudMap(input_point_cloud);

    cv::Rect2f pos2d(400, 300, 100, 30);
    cv::Point3f object_position;
    localizer->SetParamters(0.56);

    localizer->Localize(pos2d,  object_position);

    LOG(INFO) << "object 3d position: " << object_position.x << ","   << object_position.y << "," <<  object_position.z;

    cv::Point2f image_position = localizer->Project3dPointToImage(object_position);
    LOG(INFO) << "object 2d position: " << image_position.x << ","   << image_position.y;

}
void Test(int argc, char** argv) {

    Run();

}

}
}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  CHECK(!FLAGS_calibration_filepath.empty())
      << "-configuration_directory is missing.";

  CHECK(!FLAGS_cloud_map_filepath.empty())
      << "-map_configuration_basename is missing.";


  object_location::object_localize3d::Test(argc, argv);

}
