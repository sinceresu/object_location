/**************************************************************************

Copyright: YiDa

Version: 1.00

Author: LiXingren

Date: 2022-05-18

Description: Input rtsp stream. Output ros image.
             2022-05-18: First edited.

**************************************************************************/

#include "ros/ros.h"

#define __app_name__ "visible_stream"

namespace VisibleStreamNode
{
    class VisibleStream
    {
    public:
        VisibleStream();
        ~VisibleStream();

        void Init();
        void Update();

        double dRosRate = 200.0;

    private:
        cv::VideoCapture cap;
        bool bTestMode;
        std::string strUrl;
        
        ros::Publisher pubVisibleStream;
    };
}
