#pragma once


#if (defined _WIN32 || defined WINCE || defined __CYGWIN__) && defined  LIBPCL_COLOIRZE_EXPORTS
#define LIBPCL_COLOIRZE_API __declspec(dllexport)
#elif defined __GNUC__ && __GNUC__ >= 4
#  define LIBPCL_COLOIRZE_API __attribute__ ((visibility ("default")))
#else
#define LIBPCL_COLOIRZE_API
#endif

#include <memory>
#include <stdint.h>
#include <memory>
namespace person_localize2d {

class PersonLocalize2dInterface;


  /******************************************************************************
  * \fn CreatePclColorize
  * Create Time: 2020/06/02
  * Description: -
  *   创建一个新的扫描组件实例
  *
  * \param type 输入参数
  * 		扫描组件实例类型
  *
  * \return
  * 		扫描组件实例
  *
  * \note 
  *******************************************************************************/
std::shared_ptr<PersonLocalize2dInterface> CreatePerson2dLocalizer();

} // namespace person_localize2d

