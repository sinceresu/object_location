/*
 * Copyright 2017 The vehicle_detect Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once

#include <vector>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include "common/common.h"

namespace object_location {
using namespace common;

namespace object_localize3d {
  
class FloorExtractor {
 public:
  typedef struct FloorExtractParam { 
    FloorExtractParam() : 
    tilt_deg(0.f),
    sensor_height(135.0f),
    height_clip_range(3.0f),
    floor_normal_thresh(20.0f),
    use_normal_filtering(true),
    normal_filter_thresh(20),
    extract_only_floor(true),
    use_upsample_filtering(true)
    {
    };
    float tilt_deg;
    float sensor_height;
    float height_clip_range;
    float floor_normal_thresh;
    bool use_normal_filtering;
    float normal_filter_thresh;
    bool extract_only_floor;
    bool use_upsample_filtering;

  }FloorExtractParam;
  
  FloorExtractor();
  ~FloorExtractor(){};
  void SetParam(FloorExtractParam param) {
    param_ = param;
  }
  int Extract(PointCloud::Ptr input_pcl, PointCloud::Ptr output_pcl);

 private:
     /**
   * @brief filter points with non-vertical normals
   * @param cloud  input cloud
   * @return filtered cloud
   */
    PointCloud::Ptr normal_filtering(const PointCloud::Ptr &cloud) const;
    // std::vector <PointCloud::Ptr> SegPointcloud(const PointCloud::Ptr &cloud);
    PointCloud::Ptr   FilterFloor(PointCloud::Ptr input_pcl, PointCloud::Ptr non_floor_pcl);
    // int DilatePlane(PointCloud::Ptr input_pcl,  Eigen::VectorXf plane_coefficients, PointCloud::Ptr output_pcl);

    FloorExtractParam param_;

};
} // namespace object_localize3d
}  // namespace object_localize3d

